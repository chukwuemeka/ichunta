from __future__ import absolute_import, unicode_literals

import io
import os
import re
import shlex
import subprocess
import tempfile
from datetime import datetime, timedelta
from uuid import uuid4

import boto3
import cv2
import exifread
import requests
import structlog
from celery import shared_task
from django.core.exceptions import ObjectDoesNotExist
from django.utils import timezone

from .models import Medium, S3Object, Tag, TaggedMedium, Video

logger = structlog.get_logger(__name__)

LABEL_DETECTION_JOB_POLLING_INTERVAL = timedelta(seconds=180)


def calculate_video_duration(video, video_file_path):
    video_capture = cv2.VideoCapture(video_file_path)
    frame_rate = video_capture.get(cv2.CAP_PROP_FPS)
    total_num_frames = int(video_capture.get(cv2.CAP_PROP_FRAME_COUNT))
    video.duration = timedelta(milliseconds=(total_num_frames * 1000) / frame_rate)
    video.save()


def create_video_thumbnail(video, video_file_path):
    # Snatch one frame from the video
    image = None
    video_capture = cv2.VideoCapture(video_file_path)
    video_capture.set(1, 0)
    is_ok, frame = video_capture.read(1)
    if not is_ok:
        raise Exception("Failed to read frame")
    is_ok, image = cv2.imencode(".JPEG", frame)
    if not is_ok:
        raise Exception("Failed to encode frame")

    # Upload to S3
    bucket = video.s3_object.bucket
    key = f"{uuid4().hex}.jpg"
    s3 = boto3.resource("s3").meta.client
    s3.upload_fileobj(
        io.BytesIO(image.tobytes()),
        bucket,
        key,
        ExtraArgs={"CacheControl": f"max-age={timedelta(days=365).total_seconds()}"},
    )

    # Save
    video.thumbnail_s3_object = S3Object.objects.create(
        bucket=bucket,
        key=key,
        size_in_bytes=len(image.tobytes()),
    )
    video.save()


def detect_video_taken_at(video, video_file_path):
    log = logger.bind(video_id=video.id, video_file_path=video_file_path)
    rekognition = boto3.client("rekognition")

    video_capture = cv2.VideoCapture(video_file_path)
    frame_rate = video_capture.get(cv2.CAP_PROP_FPS)
    total_num_frames = int(video_capture.get(cv2.CAP_PROP_FRAME_COUNT))
    sampling_factor = 1
    for i in range(0, total_num_frames, int(frame_rate) * sampling_factor):
        video_capture.set(1, i)
        is_ok, frame = video_capture.read(1)
        if not is_ok:
            raise Exception("Failed to read frame")
        is_ok, image = cv2.imencode(".JPEG", frame)
        if not is_ok:
            raise Exception("Failed to encode frame")

        # Text detection
        response = rekognition.detect_text(Image={"Bytes": image.tobytes()})
        taken_at = None
        taken_on = None
        log.debug("Text detected from video frame", response=response)
        for text_detection in response["TextDetections"]:
            # This is a hack to "fix" cases where the decimal point is interpreted as a '-'
            detected_text = (
                text_detection["DetectedText"].replace(".", ":").replace("-", ":")
            )
            log.debug("Processing detected text", detected_text=detected_text)

            match = re.search(r"(\d{2}:\d{2}:\d{2})", detected_text)
            if match:
                t = datetime.strptime(match.group(0), "%H:%M:%S")
                if taken_at is None:
                    taken_at = t
                else:
                    taken_at = min(taken_at, t)
            match = re.search(r"(\d{4}/\d{2}/\d{2})", detected_text)
            if match:
                d = datetime.strptime(match.group(0), "%Y/%m/%d")
                if taken_on is None:
                    taken_on = d
                else:
                    taken_on = min(taken_on, d)
        # Set the timestamp
        if taken_at and taken_on:
            video.set_taken_at(
                datetime.combine(taken_on.date(), taken_at.time())
                - timedelta(seconds=i * sampling_factor)
            )
            return


def maybe_transcode_video(video):
    # Download the video file /yuck
    video_file_path = os.path.join(
        tempfile.gettempdir(), f"{uuid4().hex}.{video.s3_object.format}"
    )
    response = requests.get(video.generate_presigned_url(), allow_redirects=True)
    with open(video_file_path, "wb+") as f:
        f.write(response.content)

    # Is this already an MP4?
    if video.s3_object.format == "mp4":
        return video_file_path

    # Convert to MP4
    h264_video_file_path = os.path.join(tempfile.gettempdir(), f"{uuid4().hex}.mp4")
    subprocess.check_call(
        shlex.split(
            f"ffmpeg -y -i {video_file_path} -c:a aac -b:a 128k -c:v libx264 -crf 23 {h264_video_file_path}"
        )
    )

    # Upload H264 to S3
    id = str(video.id)
    s3 = boto3.resource("s3").meta.client
    bucket = video.s3_object.bucket
    file_extension = os.path.splitext(h264_video_file_path.lower())[1].lstrip(".")
    h264_key_name = f"{id}.{file_extension}"
    s3.upload_file(
        h264_video_file_path,
        bucket,
        h264_key_name,
        ExtraArgs={"CacheControl": f"max-age={timedelta(days=365).total_seconds()}"},
    )

    # Capture S3 object to delete
    s3_object_to_delete = video.s3_object

    # Save the new one
    video.s3_object = S3Object.objects.create(
        bucket=bucket,
        key=h264_key_name,
        size_in_bytes=os.path.getsize(h264_video_file_path),
    )
    video.save()

    s3_object_to_delete.delete()
    os.remove(video_file_path)

    return h264_video_file_path


def process_video(video, do_machine_generate_labels=False):
    log = logger.bind(video_id=video.id)
    log.debug("Processing video")

    # Transcode the video and store it locally
    log.debug("Begin transcoding video")
    h264_video_file_path = maybe_transcode_video(video)

    # Get duration
    log.debug("Begin calculating video duration")
    calculate_video_duration(video, h264_video_file_path)

    # Get timestamp
    log.debug("Begin calculating video taken_at")
    try:
        detect_video_taken_at(video, h264_video_file_path)
    except Exception:
        log.exception("Failed to calculate video taken_at")

    # Create thumbnail
    log.debug("Begin creating video's thumbnail")
    create_video_thumbnail(video, h264_video_file_path)

    # Start label detection
    if do_machine_generate_labels:
        log.debug("Start label detection job")
        rekognition = boto3.client("rekognition")
        request_token = f"{video.id}{int(datetime.utcnow().timestamp())}"
        response = rekognition.start_label_detection(
            Video={
                "S3Object": {
                    "Bucket": video.s3_object.bucket,
                    "Name": video.s3_object.key,
                }
            },
            ClientRequestToken=request_token,
            MinConfidence=70,
        )
        log.debug("Received response from start_label_detection", response=response)
        video.label_detection_job_id = response["JobId"]
        video.save()

        # Poll AWS
        log.debug("Enqueue label detection job")
        process_label_detection_job.apply_async(
            args=[video.label_detection_job_id],
            countdown=LABEL_DETECTION_JOB_POLLING_INTERVAL.total_seconds(),
        )

    # Cleanup
    os.remove(h264_video_file_path)


def process_photo(photo, do_machine_generate_labels=False):
    # Detect labels
    if do_machine_generate_labels:
        rekognition = boto3.client("rekognition")
        response = rekognition.detect_labels(
            Image={
                "S3Object": {
                    "Bucket": photo.s3_object.bucket,
                    "Name": photo.s3_object.key,
                }
            },
            MinConfidence=70,
            MaxLabels=9000,
        )
        for label in response["Labels"]:
            name = label["Name"].strip().lower()
            confidence = label["Confidence"]
            tag, _ = Tag.objects.get_or_create(name=name)
            TaggedMedium.objects.get_or_create(
                tag=tag,
                content_object=photo,
                defaults={
                    "confidence": confidence,
                    "is_machine_generated": True,
                },
            )
        photo.machine_generated_labels_at = timezone.now()
        photo.save()

    # Grab some exif tags
    response = requests.get(photo.generate_presigned_url(), allow_redirects=True)
    exif_tags = exifread.process_file(
        io.BytesIO(response.content), stop_tag="DateTimeOriginal", details=False
    )
    taken_at = datetime.strptime(
        exif_tags["EXIF DateTimeOriginal"].values, "%Y:%m:%d %H:%M:%S"
    )
    photo.set_taken_at(taken_at)
    photo.exif = exif_tags
    photo.save()


@shared_task
def process_medium(id, do_machine_generate_labels=False):
    medium = Medium.objects.get(id=id)
    if medium.is_photo:
        process_photo(
            medium.photo, do_machine_generate_labels=do_machine_generate_labels
        )
    elif medium.is_video:
        process_video(
            medium.video, do_machine_generate_labels=do_machine_generate_labels
        )
    else:
        raise NotImplementedError


@shared_task
def process_label_detection_job(label_detection_job_id):
    log = logger.bind(job_id=label_detection_job_id)
    try:
        video = Video.objects.get(
            label_detection_job_id=label_detection_job_id,
            machine_generated_labels_at=None,
        )
    except ObjectDoesNotExist:
        log.exception("Failed to find video")
        return
    log = log.bind(video_id=str(video.id))
    rekognition = boto3.client("rekognition")
    response = rekognition.get_label_detection(
        JobId=label_detection_job_id,
    )
    if response["JobStatus"] == "FAILED":
        log.error(
            "Label detection job failed", status_message=response["StatusMessage"]
        )
    elif response["JobStatus"] == "IN_PROGRESS":
        log.info("Label detection job still in progress")
        process_label_detection_job.apply_async(
            args=[label_detection_job_id],
            countdown=LABEL_DETECTION_JOB_POLLING_INTERVAL.total_seconds(),
        )
    else:
        log.info("Label detection job succeeded")
        confidence_by_name = {}
        for label in response["Labels"]:
            name = label["Label"]["Name"].strip().lower()
            confidence = label["Label"]["Confidence"]
            if name not in confidence_by_name:
                confidence_by_name[name] = confidence
            confidence_by_name[name] = max(confidence_by_name[name], confidence)
        log.debug(
            "Processed label detection response", confidence_by_name=confidence_by_name
        )
        for name, confidence in confidence_by_name.items():
            log.debug("Attaching tag", name=name, confidence=confidence)
            tag, _ = Tag.objects.get_or_create(name=name.strip().lower())
            TaggedMedium.objects.get_or_create(
                tag=tag,
                content_object=video,
                defaults={
                    "confidence": confidence,
                    "is_machine_generated": True,
                },
            )
        video.machine_generated_labels_at = timezone.now()
        video.save()
