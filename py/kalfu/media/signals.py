import boto3
from django.db.models import Subquery
from django.db.models.signals import m2m_changed, post_delete, pre_save
from django.dispatch import receiver
from django.utils import timezone

from .models import Medium, S3Object, Settings, TaggedMedium, Video


@receiver(m2m_changed, sender=Settings.hidden_tags.through)
def handle_settings_hidden_tags_m2m_changed(sender, instance, action, **kwargs):
    user = instance.user
    medium = Medium.objects.filter(camera__area__user=user)
    if action == "post_add":
        TaggedMedium.objects.filter(
            is_machine_generated=True,
            hidden_at=None,
            tag__in=instance.hidden_tags.all(),
            content_object_id__in=Subquery(medium.values("id")),
        ).update(hidden_at=timezone.now())
    elif action == "post_remove":
        TaggedMedium.objects.exclude(
            hidden_at=None, tag__in=instance.hidden_tags.all()
        ).filter(
            is_machine_generated=True,
            content_object_id__in=Subquery(medium.values("id")),
        ).update(
            hidden_at=None
        )


@receiver(post_delete, sender=S3Object)
def handle_s3_object_post_delete(sender, instance, **kwargs):
    s3 = boto3.resource("s3")
    s3.meta.client.delete_object(Bucket=instance.bucket, Key=instance.key)


@receiver(post_delete, sender=Medium)
def handle_medium_post_delete(sender, instance, **kwargs):
    instance.s3_object.delete()


@receiver(post_delete, sender=Video)
def handle_video_post_delete(sender, instance, **kwargs):
    if instance.thumbnail_s3_object:
        instance.thumbnail_s3_object.delete()


@receiver(pre_save, sender=TaggedMedium)
def handle_tagged_medium_pre_save(sender, instance, **kwargs):
    user = instance.content_object.camera.area.user
    tag = instance.tag
    settings, _ = Settings.objects.get_or_create(user=user)
    if (
        instance.is_machine_generated
        and settings.hidden_tags.filter(name=tag.name).exists()
    ):
        instance.hidden_at = timezone.now()
