import pandas as pd
import plotly.express as px
import structlog
from django.core.management.base import BaseCommand

from auth.models import User
from media.models import Medium

logger = structlog.get_logger(__name__)


class Command(BaseCommand):
    help = "Show histogram of media data"

    def add_arguments(self, parser):
        parser.add_argument("email_address")
        parser.add_argument("--tag", action="append", dest="tags")
        parser.add_argument(
            "--time-of-day", dest="do_show_per_hour", action="store_true"
        )

    def handle(self, *args, **options):
        user = User.objects.get(email_address=options["email_address"])

        # Dump the user's media into a data frame
        data_frame = pd.DataFrame()
        for medium in (
            Medium.objects.filter(
                camera__area__user=user, tags__name__in=options["tags"]
            )
            .exclude(taken_at__hour__lt=6)
            .exclude(taken_at__hour__gt=20)
            .exclude(taken_at__isnull=True)
        ):
            data_frame = data_frame.append(
                pd.DataFrame(
                    [
                        [
                            medium.s3_object.key,
                            f"{medium.camera.area.name} - {medium.camera.name}",
                            medium.taken_at,
                        ]
                    ],
                    columns=["s3_key", "camera_name", "taken_at"],
                )
            )

        # Build a downsampled data frame
        downsampled_data_frame = pd.DataFrame()
        for name, group in data_frame.groupby("camera_name"):
            downsampled_data_frame = downsampled_data_frame.append(
                group.resample("5Min", closed="right", on="taken_at").first().dropna()
            )

        # Hourly...?
        if options["do_show_per_hour"]:
            print("YO!")
            downsampled_data_frame["taken_on"] = downsampled_data_frame[
                "taken_at"
            ].dt.time
            print(downsampled_data_frame["taken_on"])

        # Show the gram
        fig = px.histogram(
            downsampled_data_frame, x="taken_at", color="camera_name", nbins=9
        )
        fig.write_html("out.html")
