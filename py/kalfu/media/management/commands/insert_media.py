import json
import os
import re
from datetime import timedelta
from uuid import NAMESPACE_URL, uuid5

import boto3
import structlog
from django.conf import settings
from django.contrib.gis.geos import Point
from django.core.management.base import BaseCommand
from pytz import timezone

from auth.models import User
from media.models import (
    IMAGE_FORMATS,
    Area,
    Camera,
    Medium,
    Photo,
    S3Object,
    Tag,
    TaggedMedium,
    Video,
)
from media.tasks import process_medium

logger = structlog.get_logger(__name__)


class Command(BaseCommand):
    help = "Insert media into the database"

    def add_arguments(self, parser):
        parser.add_argument("email_address")
        parser.add_argument("area_name")
        parser.add_argument("camera_name")
        parser.add_argument("timezone")
        parser.add_argument("latitude", type=float)
        parser.add_argument("longitude", type=float)
        parser.add_argument("media_path")
        parser.add_argument("--tags-file-path")

    def insert_media(self, file_path: str, camera: Camera, tags_by_uuid={}):
        id = uuid5(NAMESPACE_URL, file_path.lower().strip()).hex
        log = logger.bind(file_path=file_path, camera_id=str(camera.id), id=id)
        log.info("Inserting media")
        if Medium.objects.filter(id=id).exists():
            medium = Medium.objects.get(id=id)
            if not medium.tags.all():
                process_medium.apply_async(args=[medium.id])
            return

        # Upload to S3
        s3 = boto3.resource("s3")
        bucket = settings.AWS_STORAGE_BUCKET_NAME
        file_extension = os.path.splitext(file_path.lower())[1].lstrip(".")
        key = f"{id}.{file_extension}"
        s3.meta.client.upload_file(
            file_path,
            bucket,
            key,
            ExtraArgs={
                "CacheControl": f"max-age={timedelta(days=365).total_seconds()}"
            },
        )
        if file_extension in IMAGE_FORMATS:
            medium = Photo.objects.create(
                id=id,
                camera=camera,
                s3_object=S3Object.objects.create(
                    bucket=bucket,
                    key=key,
                    size_in_bytes=os.path.getsize(file_path),
                ),
            )
        else:
            medium = Video.objects.create(
                id=id,
                camera=camera,
                s3_object=S3Object.objects.create(
                    bucket=bucket,
                    key=key,
                    size_in_bytes=os.path.getsize(file_path),
                ),
            )

        # Attach migrated tags
        log.info("Attaching migrated tags", tags=tags_by_uuid.get(medium.id, []))
        for t in tags_by_uuid.get(medium.id, []):
            tag, _ = Tag.objects.get_or_create(name=t)
            TaggedMedium.objects.get_or_create(
                tag=tag,
                content_object=medium,
                defaults={
                    "is_machine_generated": False,
                },
            )

        # Celery
        process_medium.apply_async(args=[str(medium.id)])

    def handle(self, *args, **options):
        tags_by_uuid = {}
        if options["tags_file_path"]:
            with open(options["tags_file_path"], "r") as f:
                tags_by_uuid = json.load(f)
        user = User.objects.get(email_address=options["email_address"])
        area, _ = Area.objects.get_or_create(user=user, name=options["area_name"])
        camera, _ = Camera.objects.get_or_create(
            area=area,
            name=options["camera_name"],
            point=Point(options["longitude"], options["latitude"]),
            timezone=timezone(options["timezone"]),
        )
        media_file_paths = []
        for root, dirs, files in os.walk(options["media_path"], topdown=False):
            for name in files:
                file_path = os.path.join(root, name)
                if re.search(pattern=r"\.(avi|jpg|jpeg)$", string=file_path.lower()):
                    media_file_paths.append(file_path)
        for media_file_path in media_file_paths:
            self.insert_media(media_file_path, camera, tags_by_uuid=tags_by_uuid)
