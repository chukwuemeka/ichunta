import os
from datetime import timedelta
from uuid import uuid4

import boto3
import pytz
from django.conf import settings
from django.contrib.auth import get_user_model
from django.contrib.gis.db import models
from django.contrib.postgres.fields import HStoreField
from django.core.cache import cache
from django.utils.timezone import is_naive
from django.utils.translation import ugettext_lazy as _
from django_extensions.db.fields import CreationDateTimeField, ModificationDateTimeField
from taggit.managers import TaggableManager, _TaggableManager
from taggit.models import TagBase, TaggedItemBase
from timezone_field import TimeZoneField

IMAGE_FORMATS = [
    "jpeg",
    "jpg",
]
VIDEO_FORMATS = ["avi", "mp4"]


class TimeStampedModel(models.Model):
    created_at = CreationDateTimeField(_("created"))
    modified_at = ModificationDateTimeField(_("modified"))

    def save(self, **kwargs):
        self.update_modified_at = kwargs.pop(
            "update_modified_at", getattr(self, "update_modified_at", True)
        )
        super().save(**kwargs)

    class Meta:
        get_latest_by = "modified_at"
        abstract = True


class Tag(TimeStampedModel, TagBase):
    id = models.UUIDField(primary_key=True, default=uuid4, editable=False)

    def save(self, *args, **kwargs):
        self.name = self.name.strip().lower()
        super().save(*args, **kwargs)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _("Tag")
        verbose_name_plural = _("Tags")
        ordering = ["name"]


class TaggedMedium(TimeStampedModel, TaggedItemBase):
    id = models.UUIDField(primary_key=True, default=uuid4, editable=False)
    content_object = models.ForeignKey("Medium", on_delete=models.CASCADE)
    tag = models.ForeignKey(
        Tag,
        on_delete=models.CASCADE,
        related_name="%(app_label)s_%(class)s_items",
    )
    confidence = models.FloatField(null=True)
    hidden_at = models.DateTimeField(null=True)
    is_machine_generated = models.BooleanField(default=False)

    @property
    def is_hidden(self):
        return self.hidden_at is not None

    class Meta:
        verbose_name = _("Tagged medium")
        verbose_name_plural = _("Tagged media")


class TaggedMediumManager(_TaggableManager):
    def visible(self):
        return self.get_queryset(
            extra_filters={f"{self.through.tag_relname()}__hidden_at": None}
        )


class Area(TimeStampedModel):
    id = models.UUIDField(primary_key=True, default=uuid4, editable=False)
    user = models.ForeignKey(get_user_model(), on_delete=models.CASCADE)
    name = models.CharField(max_length=200)

    def __str__(self):
        return self.name

    class Meta:
        unique_together = ["user", "name"]


class Camera(TimeStampedModel):
    id = models.UUIDField(primary_key=True, default=uuid4, editable=False)
    area = models.ForeignKey(Area, on_delete=models.CASCADE)
    name = models.CharField(max_length=200)
    point = models.PointField()
    timezone = TimeZoneField()

    def __str__(self):
        return self.name

    class Meta:
        unique_together = ["area", "name"]


class S3Object(TimeStampedModel):
    id = models.UUIDField(primary_key=True, default=uuid4, editable=False)
    key = models.CharField(max_length=200)
    bucket_arn = models.CharField(max_length=200, default=settings.AWS_S3_BUCKET_ARN)
    size_in_bytes = models.IntegerField()

    @property
    def bucket_name(self):
        return self.bucket_arn.split(":")[-1]

    # Move the existing object to a new destination and delete the old object
    def move(self, destination_key, destination_bucket=settings.AWS_S3_BUCKET_NAME):
        source_bucket = self.bucket
        source_key = self.key
        if source_bucket == destination_bucket and source_key == destination_key:
            return

        # Copy source to destination
        s3 = boto3.resource("s3").meta.client
        s3.copy_object(
            Bucket=destination_bucket,
            Key=destination_key,
            CopySource={"Bucket": source_bucket, "Key": source_key},
        )

        # Save new identifiers
        self.bucket = destination_bucket
        self.key = destination_key
        self.save()

        # Delete the source object
        s3.delete_object(Bucket=source_bucket, Key=source_key)

    @property
    def format(self):
        return os.path.splitext(self.key.lower())[1].lstrip(".")

    def generate_presigned_url(self, expires_in=timedelta(seconds=3600)):
        key = f"{self.key}-presigned-url"
        presigned_url = cache.get(key)
        if presigned_url:
            return presigned_url
        s3 = boto3.resource("s3")
        presigned_url = s3.meta.client.generate_presigned_url(
            "get_object",
            Params={"Bucket": self.bucket, "Key": self.key},
            ExpiresIn=expires_in.total_seconds(),
        )
        cache.set(key, presigned_url, timeout=expires_in.total_seconds())
        return presigned_url

    @property
    def is_image(self):
        return self.format in IMAGE_FORMATS

    @property
    def is_video(self):
        return self.format in VIDEO_FORMATS

    class Meta:
        unique_together = ["key", "bucket_arn"]


class Medium(TimeStampedModel):
    id = models.UUIDField(primary_key=True, default=uuid4, editable=False)
    camera = models.ForeignKey(Camera, on_delete=models.CASCADE)
    tags = TaggableManager(through=TaggedMedium, manager=TaggedMediumManager)
    machine_generated_labels_at = models.DateTimeField(null=True)
    taken_at = models.DateTimeField(null=True)
    s3_object = models.ForeignKey(S3Object, on_delete=models.PROTECT)
    point = models.PointField()

    def generate_presigned_url(self, expires_in=timedelta(seconds=86400)):
        return self.s3_object.generate_presigned_url(expires_in=expires_in)

    def set_taken_at(self, taken_at, commit=True):
        if is_naive(taken_at):
            self.taken_at = self.camera.timezone.localize(taken_at)
        else:
            self.taken_at = taken_at
        self.taken_at = self.taken_at.astimezone(pytz.timezone("UTC"))
        if commit:
            self.save()

    @property
    def is_photo(self):
        try:
            self.photo
        except Exception:
            return False
        return True

    @property
    def is_video(self):
        try:
            self.video
        except Exception:
            return False
        return True

    def has_animal(self):
        return self.tags.filter(
            name__in=["deer", "hog", "coyote", "turkey", "dog", "human"]
        ).exists()

    class Meta:
        verbose_name = _("Medium")
        verbose_name_plural = _("Media")


class Video(Medium):
    thumbnail_s3_object = models.ForeignKey(
        S3Object, on_delete=models.SET_NULL, null=True
    )
    label_detection_job_id = models.CharField(max_length=100, blank=True)
    duration = models.DurationField(null=True)


class Photo(Medium):
    exif = HStoreField(null=True)


class Settings(TimeStampedModel):
    id = models.UUIDField(primary_key=True, default=uuid4, editable=False)
    user = models.ForeignKey(get_user_model(), on_delete=models.CASCADE)
    hidden_tags = models.ManyToManyField(Tag, blank=True)

    def __str__(self):
        return f"{self.user}"

    class Meta:
        verbose_name = _("Settings")
        verbose_name_plural = _("Settings")
