from django.contrib.gis import admin

from media.tasks import process_medium

from .models import Area, Camera, Medium, Photo, Settings, Tag, TaggedMedium, Video


class CameraAdmin(admin.GeoModelAdmin):
    list_display = [
        "id",
        "name",
        "area",
        "point",
    ]


class AreaAdmin(admin.GeoModelAdmin):
    list_display = [
        "id",
        "name",
    ]


class TagAdmin(admin.GeoModelAdmin):
    list_display = [
        "id",
        "name",
        "created_at",
    ]


class TaggedMediumAdmin(admin.GeoModelAdmin):
    list_display = [
        "id",
        "tag",
        "confidence",
        "is_machine_generated",
        "hidden_at",
        "created_at",
    ]


class MediumAdmin(admin.GeoModelAdmin):
    list_display = [
        "id",
        "machine_generated_labels_at",
        "taken_at",
        "created_at",
    ]
    fields = [
        "camera",
        "tags",
    ]
    readonly_fields = ["id", "created_at", "modified_at"]

    def get_taken_at(self, obj):
        if obj.taken_at is None:
            return None
        return obj.taken_at.astimezone(obj.camera.timezone).strftime(
            "%b. %d, %Y, %I:%M %p"
        )

    get_taken_at.short_description = "Taken At"


def process_videos(modeladmin, request, queryset):
    for video in queryset:
        if video.thumbnail_s3_object:
            video.thumbnail_s3_object.delete()
        video.thumbnail_s3_object = None
        video.label_detection_job_id = ""
        video.duration = None
        video.machine_generated_labels_at = None
        video.taken_at = None
        video.save()
        TaggedMedium.objects.filter(
            content_object=video, is_machine_generated=True
        ).delete()
        process_medium.apply_async(args=[video.id])


process_videos.short_description = "Process selected videos"


class VideoAdmin(MediumAdmin):
    list_display = [
        "id",
        "duration",
        "label_detection_job_id",
        "machine_generated_labels_at",
        "taken_at",
        "created_at",
    ]
    fields = [
        "camera",
        "tags",
        "taken_at",
    ]
    actions = [process_videos]


class PhotoAdmin(MediumAdmin):
    list_display = [
        "id",
        "machine_generated_labels_at",
        "get_taken_at",
        "created_at",
    ]
    fields = [
        "exif",
        "camera",
        "tags",
    ]


admin.site.register(Area, AreaAdmin)
admin.site.register(Camera, CameraAdmin)
admin.site.register(Medium, MediumAdmin)
admin.site.register(TaggedMedium, TaggedMediumAdmin)
admin.site.register(Tag, TagAdmin)
admin.site.register(Settings, admin.GeoModelAdmin)
admin.site.register(Photo, PhotoAdmin)
admin.site.register(Video, VideoAdmin)
