import urllib

import structlog
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponseRedirect
from django.shortcuts import redirect, render
from django.urls import reverse
from django.views.generic import ListView
from django.views.generic.base import TemplateView
from django.views.generic.edit import FormView

from .forms import CreateTaggedMediaForm, UpdateMediumTakenAtForm
from .models import Medium, TaggedMedium

logger = structlog.get_logger(__name__)


class MediumMixin:
    template_name = "media/show_medium.html"

    @property
    def medium(self):
        return Medium.objects.get(
            id=self.kwargs["medium_id"], camera__area__user=self.request.user
        )

    def get_context_data(self, **kwargs):
        medium = self.medium
        create_tagged_media_form = CreateTaggedMediaForm(
            initial={"content_object": medium.id}
        )
        update_medium_taken_at_form = UpdateMediumTakenAtForm(instance=medium)
        return {
            "area": medium.camera.area,
            "camera": medium.camera,
            "medium": medium,
            "tagged_media": TaggedMedium.objects.filter(content_object=medium).order_by(
                "tag__name"
            ),
            "create_tagged_media_form": create_tagged_media_form,
            "update_medium_taken_at_form": update_medium_taken_at_form,
        }


class ShowMediumView(MediumMixin, LoginRequiredMixin, TemplateView):
    pass


class CreateTaggedMediaView(MediumMixin, LoginRequiredMixin, FormView):
    form_class = CreateTaggedMediaForm

    def form_valid(self, form):
        form.save()
        return super().form_valid(form)

    def get_success_url(self):
        return reverse("media:show_medium", args=(self.medium.id,))

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        if self.request.method in ("POST", "PUT"):
            kwargs["data"] = kwargs["data"].copy()
            kwargs["data"]["content_object"] = self.medium.id
        return kwargs


class UpdateMediumTakenAtView(MediumMixin, LoginRequiredMixin, FormView):
    form_class = UpdateMediumTakenAtForm

    def form_valid(self, form):
        form.save()
        return super().form_valid(form)

    def get_success_url(self):
        return reverse("media:show_medium", args=(self.medium.id,))

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs.update({"instance": self.medium})
        return kwargs


@login_required
def delete_medium(request, medium_id):
    Medium.objects.get(id=medium_id, camera__area__user=request.user).delete()
    return redirect("media:list_media")


class MediumList(LoginRequiredMixin, ListView):
    template_name = "media/list_media.html"
    model = Medium
    paginate_by = 25

    def get_context_data(self):
        context_data = super().get_context_data()
        url_params = self.request.GET.copy()
        url_params.pop("page", None)
        context_data["url_params"] = urllib.parse.urlencode(url_params)
        return context_data

    def get_queryset(self):
        camera_id = self.request.GET.get("camera_id", None)
        media = (
            Medium.objects.all()
            .filter(camera__area__user=self.request.user)
            .order_by("-taken_at")
        )
        if camera_id:
            media = media.filter(camera__id=camera_id)
        return media


@login_required
def show_medium(request, medium_id):
    medium = Medium.objects.get(id=medium_id, camera__area__user=request.user)
    context = {
        "area": medium.camera.area,
        "camera": medium.camera,
        "medium": medium,
        "tagged_media": TaggedMedium.objects.filter(content_object=medium).order_by(
            "tag__name"
        ),
    }
    if request.method == "POST":
        data = request.POST.copy()
        data["content_object"] = medium.id
        form = CreateTaggedMediaForm(data)
        context["form"] = form
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse("media:show_medium", args=(medium.id,)))
        else:
            logger.error("Form validation failed", form_errors=form.errors.as_data())
    else:
        form = CreateTaggedMediaForm(initial={"content_object": medium.id})
    context["form"] = form
    return render(request, "media/show_medium.html", context)
