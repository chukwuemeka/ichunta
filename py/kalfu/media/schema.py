import mimetypes
import os
from uuid import NAMESPACE_URL, uuid5

import boto3
import graphene
import structlog
from botocore import endpoint
from django.conf import settings
from django.contrib.gis.geos import Point
from graphene_django import DjangoObjectType
from graphql_jwt.decorators import login_required
from graphql_relay import from_global_id

from .models import Area, Camera, Medium, Photo, S3Object, Video

logger = structlog.get_logger(__name__)


class NonNullConnection(graphene.Connection):
    class Meta:
        abstract = True

    @classmethod
    def __init_subclass_with_meta__(cls, node=None, name=None, **options):
        super().__init_subclass_with_meta__(node=node, name=name, **options)

        # Override the original EdgeBase type to make to `node` field required.
        class EdgeBase:
            node = graphene.Field(
                cls._meta.node,
                description="The item at the end of the edge",
                required=True,
            )
            cursor = graphene.String(
                required=True, description="A cursor for use in pagination"
            )

        # Create the edge type using the new EdgeBase.
        edge_name = cls.Edge._meta.name
        edge_bases = (
            EdgeBase,
            graphene.ObjectType,
        )
        edge = type(edge_name, edge_bases, {})
        cls.Edge = edge

        # Override the `edges` field to make it non-null list
        # of non-null edges.
        cls._meta.fields["edges"] = graphene.Field(
            graphene.NonNull(graphene.List(graphene.NonNull(cls.Edge)))
        )


class CameraNode(DjangoObjectType):
    class Meta:
        model = Camera
        name = "Camera"
        fields = ("id", "name")
        interfaces = (graphene.relay.Node,)


class AreaNode(DjangoObjectType):
    cameras = graphene.List(graphene.NonNull(CameraNode), required=True)

    def resolve_cameras(parent, *args, **kwargs):
        return Camera.objects.filter(area_id=parent.id)

    class Meta:
        model = Area
        name = "Area"
        fields = ("id", "name")
        interfaces = (graphene.relay.Node,)


class MediumNode(DjangoObjectType):
    area = graphene.NonNull(AreaNode)

    def resolve_area(parent, *args, **kwargs):
        return Area.objects.get(id=parent.camera.area.id)

    class Meta:
        model = Medium
        name = "Medium"
        fields = ("id",)
        interfaces = (graphene.relay.Node,)


class CreateMediumMutation(graphene.relay.ClientIDMutation):
    class Input:
        area_id = graphene.String(name="areaID")
        area_name = graphene.String()
        camera_id = graphene.String(name="cameraID")
        camera_name = graphene.String()
        camera_latitude = graphene.Float()
        camera_longitude = graphene.Float()
        s3_upload_key = graphene.String(required=True)

    medium = graphene.Field(MediumNode)

    @staticmethod
    def get_or_create_area(
        user_id=None, area_name=None, area_id=None, camera_id=None, **kwargs
    ):
        if area_id is not None:
            return Area.objects.get(id=from_global_id(area_id)[1])
        elif camera_id is not None:
            return Camera.objects.get(id=from_global_id(camera_id)[1]).area
        elif user_id is not None and area_name is not None:
            return Area.objects.create(user_id=user_id, name=area_name)

    @staticmethod
    def get_or_create_camera(
        area=None,
        camera_id=None,
        camera_name=None,
        camera_latitude=None,
        camera_longitude=None,
        **kwargs,
    ):
        if camera_id is not None:
            return Camera.objects.get(id=from_global_id(camera_id)[1])
        elif (
            area is not None
            and camera_name is not None
            and camera_latitude is not None
            and camera_longitude is not None
        ):
            return Camera.objects.create(
                area=area,
                name=camera_name,
                point=Point((camera_longitude, camera_latitude)),
            )

    @classmethod
    @login_required
    def mutate_and_get_payload(cls, root, info, *args, **input):
        s3_upload_key = input.get("s3_upload_key")
        mime_type = mimetypes.guess_type(s3_upload_key)[0]
        if mime_type is None:
            raise Exception("Unknown file type")
        _type = mime_type.split("/")[0]
        if _type == "image":
            do_create_photo = True
        elif _type == "video":
            do_create_photo = False
        else:
            raise Exception("Unknown file type")
        user = info.context.user
        area = cls.get_or_create_area(user_id=user.id, **input)
        camera = cls.get_or_create_camera(area=area, **input)
        normalized_s3_upload_key = os.path.normpath(s3_upload_key)
        file_extension = os.path.splitext(normalized_s3_upload_key)[1].lstrip(".")
        media_id = uuid5(NAMESPACE_URL, normalized_s3_upload_key).hex
        s3_key = f"{media_id}.{file_extension}"

        # Copy from upload directory
        s3 = boto3.client("s3", endpoint_url=settings.AWS_S3_ENDPOINT_URL)
        s3.copy_object(
            Bucket=settings.AWS_S3_BUCKET_NAME,
            Key=s3_key,
            CopySource={"Bucket": settings.AWS_S3_BUCKET_NAME, "Key": s3_upload_key},
        )

        # Create medium
        medium = (
            (Photo if do_create_photo else Video)
            .objects.create(
                id=media_id,
                camera=camera,
                point=camera.point,
                s3_object=S3Object.objects.create(
                    bucket_arn=settings.AWS_S3_BUCKET_ARN,
                    key=s3_key,
                    size_in_bytes=0,
                ),
            )
            .medium_ptr
        )

        # Delete object from upload directory
        s3.delete_object(Bucket=settings.AWS_S3_BUCKET_NAME, Key=s3_upload_key)

        return CreateMediumMutation(medium=medium)


class GetCurrentUserAreasConnection(NonNullConnection):
    class Meta:
        node = AreaNode


class Query(graphene.ObjectType):
    node = graphene.relay.Node.Field()
    get_current_user_areas = graphene.relay.ConnectionField(
        GetCurrentUserAreasConnection, required=True
    )

    def resolve_get_current_user_areas(root, info, *args, **kwargs):
        return Area.objects.filter(user_id=info.context.user.id)


class Mutation(graphene.ObjectType):
    create_medium = CreateMediumMutation.Field()


schema = graphene.Schema(query=Query, mutation=Mutation)
