from pytest_factoryboy import register

from cognito.factories import UserFactory

from ..factories import AreaFactory, CameraFactory

register(AreaFactory)
register(CameraFactory)
register(UserFactory)
