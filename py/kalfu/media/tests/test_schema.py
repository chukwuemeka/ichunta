import json
import os
from string import Template

import boto3
import pytest
import structlog
from django.conf import settings
from django.contrib.gis.geos import Point
from graphene_django.utils.testing import graphql_query
from graphql_relay import to_global_id
from graphql_relay.node.node import from_global_id

from media.models import Area, Camera, Medium, Photo, S3Object, Video

logger = structlog.get_logger(__name__)


@pytest.fixture
def setup_s3_bucket(s3_resource):
    bucket = s3_resource.Bucket(settings.AWS_S3_BUCKET_NAME)
    bucket.create()
    yield
    for object in bucket.objects.all():
        object.delete()
    bucket.delete()


@pytest.fixture
def s3_resource(configure_aws):
    yield boto3.resource("s3", endpoint_url=settings.AWS_S3_ENDPOINT_URL)


@pytest.fixture
def s3_client(s3_resource):
    yield s3_resource.meta.client


@pytest.fixture
def upload_to_s3(s3_resource):
    def func(key):
        s3_resource.Object(settings.AWS_S3_BUCKET_NAME, key).put(Body=b"foo")

    return func


@pytest.fixture
def configure_aws(settings):
    settings.AWS_S3_ENDPOINT_URL = os.environ["MOTO_URL"]
    settings.AWS_ACCESS_KEY_ID = "testing"
    settings.AWS_SECRET_ACCESS_KEY = "testing"
    settings.AWS_SECURITY_TOKEN = "testing"
    settings.AWS_SESSION_TOKEN = "testing"


@pytest.fixture
def authorized_client(client, user):
    client.force_login(user)
    return client


@pytest.fixture
def unauthorized_client_query():
    def func(*args, **kwargs):
        return graphql_query(*args, **kwargs, graphql_url="/gql/")

    return func


@pytest.fixture
def authorized_client_query(authorized_client):
    def func(*args, **kwargs):
        return graphql_query(
            *args, **kwargs, client=authorized_client, graphql_url="/gql/"
        )

    return func


@pytest.mark.django_db(transaction=True)
class TestGetCurrentUserAreasConnection:
    def test_fetch_camera_data(
        self, authorized_client_query, camera_factory, area_factory, user
    ):
        [camera_factory() for i in range(10)]
        expected_camera = camera_factory(area=area_factory(user=user))
        query = """
query {
  getCurrentUserAreas {
    edges {
      node {
        id
        name
        cameras {
          id
          name
        }
      }
    }
  }
}
      """
        response = authorized_client_query(
            query,
        )
        assert response.status_code == 200, response.content
        payload = json.loads(response.content)
        assert "errors" not in payload
        assert len(payload.get("data").get("getCurrentUserAreas").get("edges")) == 1
        assert (
            len(
                payload.get("data")
                .get("getCurrentUserAreas")
                .get("edges")[0]
                .get("node")
                .get("cameras")
            )
            == 1
        )
        assert from_global_id(
            payload.get("data")
            .get("getCurrentUserAreas")
            .get("edges")[0]
            .get("node")
            .get("cameras")[0]
            .get("id")
        )[1] == str(expected_camera.id)
        assert payload.get("data").get("getCurrentUserAreas").get("edges")[0].get(
            "node"
        ).get("cameras")[0].get("name") == str(expected_camera.name)

    def test_fetch_only_user_areas(self, authorized_client_query, area_factory, user):
        [area_factory() for i in range(10)]
        expected_area = area_factory(user=user)
        query = """
query {
  getCurrentUserAreas {
    edges {
      node {
        id
        name
        cameras {
          id
          name
        }
      }
    }
  }
}
      """
        response = authorized_client_query(
            query,
        )
        print(json.loads(response.content))
        assert response.status_code == 200, response.content
        payload = json.loads(response.content)
        assert "errors" not in payload
        assert len(payload.get("data").get("getCurrentUserAreas").get("edges")) == 1
        assert from_global_id(
            payload.get("data")
            .get("getCurrentUserAreas")
            .get("edges")[0]
            .get("node")
            .get("id")
        )[1] == str(expected_area.id)
        assert (
            payload.get("data")
            .get("getCurrentUserAreas")
            .get("edges")[0]
            .get("node")
            .get("name")
            == expected_area.name
        )


@pytest.mark.django_db(transaction=True)
@pytest.mark.usefixtures("setup_s3_bucket")
class TestCreateMedium:
    def test_returns_area(self, authorized_client_query, user, upload_to_s3):
        upload_to_s3("foo.png")
        query = Template(
            """
mutation{
  createMedium(input: { 
    areaName: "$area_name", 
    cameraName: "$camera_name", 
    cameraLatitude: $camera_latitude, 
    cameraLongitude: $camera_longitude, 
    s3UploadKey: "$s3_upload_key",
  }) {
    medium{
      id
      area {
        id
        name
      }
    }
  }
}
      """
        ).substitute(
            area_name="foo",
            s3_upload_key="foo.png",
            camera_name="foo",
            camera_latitude=0,
            camera_longitude=0,
        )
        response = authorized_client_query(
            query,
        )
        assert response.status_code == 200, response.content
        payload = json.loads(response.content)
        assert "errors" not in payload
        assert from_global_id(
            payload.get("data").get("createMedium").get("medium").get("area").get("id")
        )[1] == str(Area.objects.first().id)
        assert (
            payload.get("data")
            .get("createMedium")
            .get("medium")
            .get("area")
            .get("name")
            == "foo"
        )

    def test_unauthorized_user(self, unauthorized_client_query, camera, upload_to_s3):
        upload_to_s3("foo.png")
        assert not Medium.objects.filter(camera_id=camera.id).exists()
        query = Template(
            """
mutation{
  createMedium(input: { cameraID:"$camera_id", s3UploadKey:"$s3_upload_key" }) {
    medium{
      id
    }
  }
}
      """
        ).substitute(
            s3_upload_key="foo.png", camera_id=to_global_id("CameraNode", camera.id)
        )
        response = unauthorized_client_query(
            query,
        )
        assert response.status_code == 200
        assert "errors" in json.loads(response.content)
        assert not Medium.objects.filter(camera_id=camera.id).exists()

    def test_missing_camera_id(self, authorized_client_query, upload_to_s3):
        upload_to_s3("foo.png")
        query = Template(
            """
mutation{
  createMedium(input: { s3UploadKey:"$s3_upload_key" }) {
    medium{
      id
    }
  }
}
      """
        ).substitute(
            s3_upload_key="foo.png",
        )
        response = authorized_client_query(
            query,
        )
        assert response.status_code == 200
        assert "errors" in json.loads(response.content)

    def test_create_photo_with_new_area_and_new_camera(
        self, authorized_client_query, user, upload_to_s3
    ):
        upload_to_s3("foo.png")
        assert not Area.objects.all().exists()
        assert not Medium.objects.all().exists()
        assert not Camera.objects.all().exists()
        query = Template(
            """
mutation{
  createMedium(input: { 
    areaName: "$area_name", 
    cameraName: "$camera_name", 
    cameraLatitude: $camera_latitude, 
    cameraLongitude: $camera_longitude, 
    s3UploadKey: "$s3_upload_key",
  }) {
    medium{
      id
    }
  }
}
      """
        ).substitute(
            area_name="foo",
            s3_upload_key="foo.png",
            camera_name="foo",
            camera_latitude=0,
            camera_longitude=0,
        )
        response = authorized_client_query(
            query,
        )
        assert response.status_code == 200, response.content
        data = json.loads(response.content)
        assert "errors" not in data

        # Check area was created
        qs = Area.objects.filter(name="foo", user=user)
        assert qs.count() == 1
        area = qs.first()

        # Check camera was created
        qs = Camera.objects.filter(area=area, name="foo", point=Point(0, 0))
        assert qs.count() == 1
        camera = qs.first()

        # Check medium was created
        assert Medium.objects.filter(camera=camera, point=Point(0, 0)).count() == 1

    def test_create_photo_with_existing_area(
        self, authorized_client_query, area, upload_to_s3
    ):
        upload_to_s3("foo.png")
        assert not Medium.objects.filter(camera__area_id=area.id).exists()
        assert not Camera.objects.filter(area_id=area.id).exists()
        query = Template(
            """
mutation{
  createMedium(input: { 
    areaID: "$area_id", 
    cameraName: "$camera_name", 
    cameraLatitude: $camera_latitude, 
    cameraLongitude: $camera_longitude, 
    s3UploadKey: "$s3_upload_key",
  }) {
    medium{
      id
    }
  }
}
      """
        ).substitute(
            area_id=to_global_id("AreaNode", area.id),
            s3_upload_key="foo.png",
            camera_name="foo",
            camera_latitude=0,
            camera_longitude=0,
        )
        response = authorized_client_query(
            query,
        )
        assert response.status_code == 200, response.content
        data = json.loads(response.content)
        assert "errors" not in data
        assert (
            Camera.objects.filter(
                area_id=area.id, name="foo", point=Point(0, 0)
            ).count()
            == 1
        )
        assert (
            Medium.objects.filter(camera__area_id=area.id, point=Point(0, 0)).count()
            == 1
        )

    def test_create_photo_with_existing_camera(
        self, authorized_client_query, camera, upload_to_s3
    ):
        upload_to_s3("foo.png")
        assert not Medium.objects.filter(camera_id=camera.id).exists()
        query = Template(
            """
mutation{
  createMedium(input: { cameraID:"$camera_id", s3UploadKey:"$s3_upload_key" }) {
    medium{
      id
    }
  }
}
      """
        ).substitute(
            s3_upload_key="foo.png", camera_id=to_global_id("CameraNode", camera.id)
        )
        response = authorized_client_query(
            query,
        )
        assert response.status_code == 200, response.content
        data = json.loads(response.content)
        assert "errors" not in data
        qs = Medium.objects.filter(camera_id=camera.id)
        assert qs.count() == 1
        medium = qs.first()
        assert medium.point == camera.point
        assert medium.s3_object is not None
        assert Photo.objects.filter(medium_ptr=medium).exists()

    def test_s3_object_created(
        self, authorized_client_query, camera, upload_to_s3, s3_client
    ):
        upload_to_s3("foo.png")
        assert not S3Object.objects.filter().exists()
        query = Template(
            """
mutation{
  createMedium(input: { cameraID:"$camera_id", s3UploadKey:"$s3_upload_key" }) {
    medium{
      id
    }
  }
}
      """
        ).substitute(
            s3_upload_key="foo.png", camera_id=to_global_id("CameraNode", camera.id)
        )
        response = authorized_client_query(
            query,
        )
        assert response.status_code == 200, response.content
        data = json.loads(response.content)
        assert "errors" not in data
        qs = S3Object.objects.filter(medium__camera_id=camera.id)
        assert qs.count() == 1
        s3_object = qs.first()
        assert s3_client.get_object(Bucket=s3_object.bucket_name, Key=s3_object.key)

    def test_create_video_with_existing_camera(
        self, authorized_client_query, camera, upload_to_s3
    ):
        s3_upload_key = "foo.mov"
        upload_to_s3(s3_upload_key)
        assert not Medium.objects.filter(camera_id=camera.id).exists()
        query = Template(
            """
mutation{
  createMedium(input: { cameraID:"$camera_id", s3UploadKey:"$s3_upload_key" }) {
    medium{
      id
    }
  }
}
      """
        ).substitute(
            s3_upload_key="foo.mov", camera_id=to_global_id("CameraNode", camera.id)
        )
        response = authorized_client_query(
            query,
        )
        assert response.status_code == 200, response.content
        data = json.loads(response.content)
        assert "errors" not in data
        qs = Medium.objects.filter(camera_id=camera.id)
        assert qs.count() == 1
        medium = qs.first()
        assert medium.point == camera.point
        assert medium.s3_object is not None
        assert Video.objects.filter(medium_ptr=medium).exists()
