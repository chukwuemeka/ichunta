from django.core.exceptions import ValidationError
from django.forms import CharField, DateTimeField, Form, ModelForm, UUIDField
from django.forms.widgets import DateTimeInput, TextInput
from django.utils.timezone import make_naive

from .models import Medium, Tag, TaggedMedium


class UpdateMediumTakenAtForm(ModelForm):
    taken_at = DateTimeField(
        widget=DateTimeInput(
            attrs={
                "class": "form-control form-control-lg",
            }
        ),
    )

    def clean(self):
        cleaned_data = super().clean()
        cleaned_data["taken_at"] = make_naive(cleaned_data["taken_at"])
        return cleaned_data

    def save(self, commit=True):
        self.instance.set_taken_at(self.cleaned_data["taken_at"], commit=commit)

    class Meta:
        model = Medium
        fields = ["taken_at"]


class CreateTaggedMediaForm(Form):
    tags = CharField(
        max_length=200,
        widget=TextInput(
            attrs={
                "class": "form-control form-control-lg",
            }
        ),
    )
    content_object = UUIDField(
        widget=TextInput(
            attrs={"class": "form-control form-control-lg", "disabled": True}
        )
    )

    def clean_content_object(self):
        return Medium.objects.get(id=self.cleaned_data["content_object"])

    def clean_tags(self):
        tags = self.cleaned_data["tags"].split(",")
        return [Tag.objects.get_or_create(name=t.strip().lower())[0] for t in tags]

    def clean(self):
        cleaned_data = super().clean()
        for tag in cleaned_data["tags"]:
            if TaggedMedium.objects.filter(
                tag=tag, content_object=cleaned_data["content_object"]
            ).exists():
                raise ValidationError("Tagged medium already exists")
        return cleaned_data

    def save(self):
        for t in self.cleaned_data["tags"]:
            TaggedMedium.objects.create(
                tag=t, content_object=self.cleaned_data["content_object"]
            )
