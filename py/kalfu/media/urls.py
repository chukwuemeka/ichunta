from django.urls import path

from . import views

app_name = "media"
urlpatterns = [
    path("media/", views.MediumList.as_view(), name="list_media"),
    path("media/<str:medium_id>/delete/", views.delete_medium, name="delete_medium"),
    path(
        "media/<str:medium_id>/show/",
        views.ShowMediumView.as_view(),
        name="show_medium",
    ),
    path(
        "media/<str:medium_id>/create-tag/",
        views.CreateTaggedMediaView.as_view(),
        name="create_tagged_medium",
    ),
    path(
        "media/<str:medium_id>/update-medium-taken-at/",
        views.UpdateMediumTakenAtView.as_view(),
        name="update_medium_taken_at",
    ),
]
