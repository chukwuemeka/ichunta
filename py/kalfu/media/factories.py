import factory
import pytz
from django.contrib.gis.geos import Point
from faker import Faker

from cognito.factories import UserFactory

from .models import Area, Camera, Medium, S3Object, Settings, Tag, TaggedMedium

fake = Faker()
Faker.seed(0)


class AreaFactory(factory.django.DjangoModelFactory):
    user = factory.SubFactory(UserFactory)
    name = factory.Faker("word")

    class Meta:
        model = Area


class CameraFactory(factory.django.DjangoModelFactory):
    area = factory.SubFactory(AreaFactory)
    name = factory.Faker("word")
    point = Point((fake.longitude(), fake.latitude()))
    timezone = pytz.timezone("US/Eastern")

    class Meta:
        model = Camera


class S3ObjectFactory(factory.django.DjangoModelFactory):
    key = factory.Faker("word")
    bucket = factory.Faker("word")
    size_in_bytes = factory.Faker("random_int")

    class Meta:
        model = S3Object


class MediumFactory(factory.django.DjangoModelFactory):
    camera = factory.SubFactory(CameraFactory)
    s3_object = factory.SubFactory(S3ObjectFactory)

    class Meta:
        model = Medium


class SettingsFactory(factory.django.DjangoModelFactory):
    user = factory.SubFactory(UserFactory)

    class Meta:
        model = Settings


class TagFactory(factory.django.DjangoModelFactory):
    name = factory.Faker("word")

    class Meta:
        model = Tag


class TaggedMediumFactory(factory.django.DjangoModelFactory):
    tag = factory.SubFactory(TagFactory)
    content_object = factory.SubFactory(MediumFactory)

    class Meta:
        model = TaggedMedium
