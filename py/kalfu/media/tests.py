from datetime import datetime

import pytz
from django.test import TestCase
from django.utils import timezone
from django.utils.dateparse import parse_date
from django.utils.timezone import is_aware, is_naive

from .factories import (
    AreaFactory,
    CameraFactory,
    MediumFactory,
    SettingsFactory,
    TagFactory,
    TaggedMediumFactory,
)
from .forms import CreateTaggedMediaForm, UpdateMediumTakenAtForm
from .models import TaggedMedium


class UpdateMediumTakenAtFormTestCase(TestCase):
    def test_sets_taken_at_to_timezone_of_camera(self):
        expected = pytz.utc.localize(
            datetime.strptime("08/23/1986 15:00:00", "%m/%d/%Y %H:%M:%S")
        )
        taken_at_text = "1986-08-23 08:00:00"
        camera = CameraFactory(timezone=pytz.timezone("US/Pacific"))
        medium = MediumFactory(camera=camera)
        form = UpdateMediumTakenAtForm(
            data={"taken_at": taken_at_text}, instance=medium
        )
        self.assertTrue(form.is_valid())
        form.save()
        medium.refresh_from_db()
        self.assertEqual(medium.taken_at, expected)
        self.assertEqual(medium.taken_at.utcoffset().total_seconds(), 0)


class TaggedMediumFormTestCase(TestCase):
    def test_creating_multiple_tags(self):
        medium = MediumFactory()
        form = CreateTaggedMediaForm(
            data={"content_object": medium.id, "tags": "over9000,vegeta"}
        )
        self.assertTrue(form.is_valid())
        self.assertTrue(TaggedMedium.objects.all().count() == 0)
        form.save()
        self.assertTrue(TaggedMedium.objects.all().count() == 2)
        self.assertTrue(
            TaggedMedium.objects.filter(
                tag__name="over9000", content_object=medium
            ).exists()
        )
        self.assertTrue(
            TaggedMedium.objects.filter(
                tag__name="vegeta", content_object=medium
            ).exists()
        )

    def test_does_not_allow_duplicates(self):
        medium = MediumFactory()
        tag = TagFactory()
        TaggedMediumFactory.create(
            content_object=medium,
            tag=tag,
        )
        form = CreateTaggedMediaForm(
            data={"content_object": medium.id, "tags": tag.name}
        )
        self.assertFalse(form.is_valid())

    def test_creates_a_new_tagged_medium(self):
        medium = MediumFactory()
        form = CreateTaggedMediaForm(
            data={"content_object": medium.id, "tags": "over9000"}
        )
        self.assertTrue(form.is_valid())
        self.assertTrue(TaggedMedium.objects.all().count() == 0)
        form.save()
        self.assertTrue(TaggedMedium.objects.all().count() == 1)
        self.assertTrue(
            TaggedMedium.objects.filter(
                tag__name="over9000", content_object=medium
            ).exists()
        )


class MediumTestCase(TestCase):
    def test_set_taken_at(self):
        camera = CameraFactory(timezone=pytz.timezone("US/Pacific"))
        medium = MediumFactory.create(camera=camera)
        expected = pytz.utc.localize(
            datetime.strptime("08/23/1986 15:00:00", "%m/%d/%Y %H:%M:%S")
        )
        medium.set_taken_at(datetime.fromisoformat("1986-08-23 08:00:00"))
        self.assertEqual(medium.taken_at, expected)
        self.assertEqual(medium.taken_at.utcoffset().total_seconds(), 0)

    def test_getting_visible_tags_given_multiple_media(self):
        medium = MediumFactory.create()
        TaggedMediumFactory.create(
            content_object=medium,
            tag=TagFactory.create(),
            is_machine_generated=True,
            hidden_at=timezone.now(),
        )
        tag = TagFactory.create()
        TaggedMediumFactory.create(
            content_object=medium,
            tag=tag,
            is_machine_generated=True,
        )
        TaggedMediumFactory.create(
            content_object=MediumFactory.create(),
            tag=tag,
            is_machine_generated=True,
        )
        self.assertTrue(medium.tags.visible().count() == 1)

    def test_getting_visible_tags(self):
        medium = MediumFactory.create()
        TaggedMediumFactory.create(
            content_object=medium,
            tag=TagFactory.create(),
            is_machine_generated=True,
            hidden_at=timezone.now(),
        )
        TaggedMediumFactory.create(
            content_object=medium,
            tag=TagFactory.create(),
            is_machine_generated=True,
        )
        self.assertTrue(medium.tags.visible().count() == 1)


class TaggedMediumTestCase(TestCase):
    def test_human_generated_tag_does_not_get_overwritten(self):
        tag = TagFactory.create()
        medium = MediumFactory.create()

        # Human tag gets written
        tagged_medium = TaggedMediumFactory.create(
            content_object=medium, tag=tag, is_machine_generated=False
        )

        # Machine attaches the same tag
        tagged_medium, _ = TaggedMedium.objects.get_or_create(
            tag=tag,
            content_object=medium,
            defaults={
                "confidence": 9000.0,
                "is_machine_generated": True,
            },
        )

        self.assertFalse(tagged_medium.is_machine_generated)
        self.assertIsNone(tagged_medium.confidence)


class SettingsTestCase(TestCase):
    def setUp(self):
        # Create stuff
        self.area = AreaFactory.create()
        self.camera = CameraFactory.create(area=self.area)
        self.medium = MediumFactory.create(camera=self.camera)

        # Create settings for the user
        user = self.medium.camera.area.user
        self.settings = SettingsFactory.create(user=user)

        # Create a tag
        self.tag = TagFactory.create()

    def test_hidden_tags_only_apply_to_machine_tags(self):
        self.settings.hidden_tags.add(self.tag)
        media = MediumFactory.create(camera=self.camera)
        tagged_medium = TaggedMediumFactory.create(
            content_object=media, tag=self.tag, is_machine_generated=False
        )
        self.assertFalse(tagged_medium.is_hidden)

    def test_new_machine_tagged_medium_gets_automatically_hidden(self):
        self.settings.hidden_tags.add(self.tag)
        media = MediumFactory.create(camera=self.camera)
        tagged_medium = TaggedMediumFactory.create(
            content_object=media, tag=self.tag, is_machine_generated=True
        )
        self.assertTrue(tagged_medium.is_hidden)

    def test_adding_a_hidden_tag_hides_all_existing_machine_media_tags(self):
        tagged_medium = TaggedMediumFactory.create(
            content_object=self.medium, tag=self.tag, is_machine_generated=True
        )
        self.assertFalse(tagged_medium.is_hidden)

        self.settings.hidden_tags.add(self.tag)
        self.settings.save()
        tagged_medium.refresh_from_db()
        self.assertTrue(tagged_medium.is_hidden)

    def test_removing_a_hidden_tag_unhides_all_existing_machine_media_tags(self):
        tagged_medium = TaggedMediumFactory.create(
            content_object=self.medium,
            tag=self.tag,
            is_machine_generated=True,
        )
        self.assertFalse(tagged_medium.is_hidden)
        self.settings.hidden_tags.add(self.tag)
        self.settings.save()
        tagged_medium.refresh_from_db()
        self.assertTrue(tagged_medium.is_hidden)
        self.settings.hidden_tags.remove(self.tag)
        self.settings.save()
        tagged_medium.refresh_from_db()
        self.assertFalse(tagged_medium.is_hidden)
