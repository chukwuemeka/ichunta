import json
from urllib.parse import urljoin

import jwt
import structlog
from django.conf import settings
from django.contrib import auth
from django.contrib.auth import load_backend
from django.core.exceptions import ImproperlyConfigured
from django.utils.deprecation import MiddlewareMixin
from jwt import PyJWKClient

from cognito.backends import CognitoUserBackend

logger = structlog.get_logger(__name__)


class CognitoUserMiddleware(MiddlewareMixin):
    header = "HTTP_AUTHORIZATION"
    force_logout_if_no_header = True

    def process_request(self, request):
        # AuthenticationMiddleware is required so that request.user exists.
        logger.debug(
            "Processing request",
            headers=json.dumps(
                [
                    {"Header": header, "Value": value}
                    for header, value in request.META.items()
                    if header.startswith("HTTP_")
                ]
            ),
        )
        if not hasattr(request, "user"):
            raise ImproperlyConfigured(
                "The Django remote user auth middleware requires the"
                " authentication middleware to be installed.  Edit your"
                " MIDDLEWARE setting to insert"
                " 'django.contrib.auth.middleware.AuthenticationMiddleware'"
                " before the RemoteUserMiddleware class."
            )
        try:
            access_token = request.META[self.header].split()[-1]
            logger.debug("Found access token", access_token=access_token)
        except KeyError:
            logger.debug("Did not find access token")
            # If specified header doesn't exist then remove any existing
            # authenticated remote-user, or return (leaving request.user set to
            # AnonymousUser by the AuthenticationMiddleware).
            if self.force_logout_if_no_header and request.user.is_authenticated:
                self._remove_invalid_user(request)
            return
        # If the user is already authenticated and that user is the user we are
        # getting passed in the headers, then the correct user is already
        # persisted in the session and we don't need to continue.
        username = self.get_username(access_token)
        logger.debug("Fetched username", username=username)
        if request.user.is_authenticated and self.is_access_token_valid(access_token):
            logger.debug("User is already authenticted")
            if request.user.get_username() == self.clean_username(username, request):
                return
            else:
                # An authenticated user is associated with the request, but
                # it does not match the authorized user in the header.
                self._remove_invalid_user(request)

        # We are seeing this user for the first time in this session, attempt
        # to authenticate the user.
        logger.debug("Authenticating user", access_token=access_token)
        user = auth.authenticate(request, access_token=access_token)
        logger.debug("Authenticated user", user=user)
        if user:
            # User is valid.  Set request.user and persist user in the session
            # by logging the user in.
            request.user = user
            auth.login(request, user)

    def _decode_access_token(self, access_token):
        url = urljoin(settings.AWS_COGNITO_USER_POOL_ENDPOINT, ".well-known/jwks.json")
        logger.debug("Decoding access token", access_token=access_token, url=url)
        jwks_client = PyJWKClient(url, cache_keys=True, max_cached_keys=1000000)
        signing_key = jwks_client.get_signing_key_from_jwt(access_token)
        return jwt.decode(
            access_token,
            signing_key.key,
            algorithms=["RS256"],
            # audience=settings.AWS_COGNITO_USER_POOL_CLIENT_ID,
            issuer=settings.AWS_COGNITO_USER_POOL_ENDPOINT.rstrip("/"),
            options={"verify_exp": True, "verify_aud": True, "verify_iss": True},
        )

    def get_username(self, access_token):
        return self._decode_access_token(access_token)["sub"]

    def is_access_token_valid(self, access_token):
        self._decode_access_token(access_token)
        return True

    def clean_username(self, username, request):
        """
        Allow the backend to clean the username, if the backend defines a
        clean_username method.
        """
        backend_str = request.session[auth.BACKEND_SESSION_KEY]
        backend = auth.load_backend(backend_str)
        try:
            username = backend.clean_username(username)
        except AttributeError:  # Backend has no clean_username method.
            pass
        return username

    def _remove_invalid_user(self, request):
        """
        Remove the current authenticated user in the request which is invalid
        but only if the user is authenticated via the CognitoUserBackend.
        """
        try:
            stored_backend = load_backend(
                request.session.get(auth.BACKEND_SESSION_KEY, "")
            )
        except ImportError:
            auth.logout(request)
        else:
            if isinstance(stored_backend, CognitoUserBackend):
                auth.logout(request)
