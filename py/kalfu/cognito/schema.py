import graphene
from django.contrib.gis.geos import Point
from graphene_django import DjangoObjectType

from .models import User


class UserNode(DjangoObjectType):
    class Meta:
        model = User
        fields = ("id",)
        interfaces = (graphene.relay.Node,)


class Query(graphene.ObjectType):
    node = graphene.relay.Node.Field()


class Mutation(graphene.ObjectType):
    pass


schema = graphene.Schema(query=Query, mutation=Mutation)
