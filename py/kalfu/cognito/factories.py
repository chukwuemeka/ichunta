import factory

from .models import User


class UserFactory(factory.django.DjangoModelFactory):
    username = factory.Faker("email")
    email_address = factory.Faker("email")

    class Meta:
        model = User
