from uuid import uuid4

from django.contrib.auth.base_user import AbstractBaseUser
from django.contrib.auth.models import PermissionsMixin, UserManager
from django.contrib.auth.validators import UnicodeUsernameValidator
from django.db import models
from django.utils.translation import ugettext_lazy as _
from django_extensions.db.fields import CreationDateTimeField, ModificationDateTimeField


class TimeStampedModel(models.Model):
    created_at = CreationDateTimeField(_("created"))
    modified_at = ModificationDateTimeField(_("modified"))

    def save(self, **kwargs):
        self.update_modified_at = kwargs.pop(
            "update_modified_at", getattr(self, "update_modified_at", True)
        )
        super().save(**kwargs)

    class Meta:
        get_latest_by = "modified_at"
        abstract = True


class User(AbstractBaseUser, PermissionsMixin, TimeStampedModel):
    EMAIL_FIELD = "email_address"
    USERNAME_FIELD = "username"
    username_validator = UnicodeUsernameValidator()
    id = models.UUIDField(primary_key=True, default=uuid4, editable=False)
    username = models.CharField(
        _("username"),
        max_length=150,
        unique=True,
        help_text=_(
            "Required. 150 characters or fewer. Letters, digits and @/./+/-/_ only."
        ),
        validators=[username_validator],
        error_messages={
            "unique": _("A user with that username already exists."),
        },
    )
    email_address = models.EmailField(_("email address"), blank=True)
    is_staff = models.BooleanField(
        _("staff status"),
        default=False,
        help_text=_("Designates whether the user can log into this admin site."),
    )
    is_active = models.BooleanField(
        _("active"),
        default=True,
        help_text=_(
            "Designates whether this user should be treated as active. "
            "Unselect this instead of deleting accounts."
        ),
    )
    objects = UserManager()

    def clean(self):
        super().clean()
        self.email_address = self.__class__.objects.normalize_email(self.email_address)

    class Meta:
        verbose_name = _("user")
        verbose_name_plural = _("users")
