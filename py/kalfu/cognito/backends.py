import boto3
import structlog
from django.contrib.auth import get_user_model
from django.contrib.auth.backends import ModelBackend
from django.db.models.base import Model

from customtypes.boto3 import cognito

logger = structlog.get_logger(__name__)


class CognitoUserBackend(ModelBackend):
    def authenticate(self, request, access_token=None, **kwargs):
        logger.debug("Authenticating user", access_token=access_token)
        if not access_token:
            return
        try:
            cognito_user = self.get_cognito_user(access_token)
            logger.debug("Fetched user", cognito_user=cognito_user)
        except:
            logger.exception("Failed to retrieve Cognito user")
            return
        UserModel = get_user_model()
        user, created = UserModel._default_manager.get_or_create(
            **{UserModel.USERNAME_FIELD: self.clean_username(cognito_user["Username"])}
        )
        if created:
            user = self.configure_user(user, cognito_user)
        return user

    def clean_username(self, username):
        return username

    def configure_user(self, user: Model, cognito_user: cognito.GetUserResponse):
        UserModel = get_user_model()
        for user_attribute in cognito_user["UserAttributes"]:
            if user_attribute["Name"] == "email":
                setattr(user, UserModel.EMAIL_FIELD, user_attribute["Value"])
        user.save()
        return user

    @staticmethod
    def get_cognito_user(access_token: str) -> cognito.GetUserResponse:
        client = boto3.client("cognito-idp")
        return client.get_user(AccessToken=access_token)
