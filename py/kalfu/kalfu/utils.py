from taggit.utils import _parse_tags


def parse_tags(tag_string):
    return [t.lower() for t in _parse_tags(tag_string)]
