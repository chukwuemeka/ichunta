import graphene

from media import schema as media_schema


class Query(media_schema.Query, graphene.ObjectType):
    node = graphene.relay.Node.Field()


class Mutation(media_schema.Mutation, graphene.ObjectType):
    pass


schema = graphene.Schema(query=Query, mutation=Mutation)
