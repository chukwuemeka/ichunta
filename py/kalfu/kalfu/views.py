from django.contrib.auth.mixins import LoginRequiredMixin
from django.http.response import JsonResponse
from django.views import View
from graphene_django.views import GraphQLView


class LoginRequiredGraphQLView(LoginRequiredMixin, GraphQLView):
    pass
