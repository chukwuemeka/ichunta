from django.contrib import admin
from django.urls import include, path
from django.views.decorators.csrf import csrf_exempt
from graphene_django.views import GraphQLView

from kalfu.views import LoginRequiredGraphQLView

urlpatterns = [
    path("admin/", admin.site.urls),
    path("media/", include("media.urls")),
    path("gql/", csrf_exempt(GraphQLView.as_view(graphiql=True))),
]
