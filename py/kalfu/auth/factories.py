import factory

from .models import User


class UserFactory(factory.django.DjangoModelFactory):
    username = factory.Faker("username")

    class Meta:
        model = User
