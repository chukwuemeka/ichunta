from uuid import uuid4

from django.contrib.auth.models import AbstractUser, BaseUserManager
from django.db import models
from django.utils.translation import ugettext_lazy as _
from django_extensions.db.fields import (
    AutoSlugField,
    CreationDateTimeField,
    ModificationDateTimeField,
)


class TimeStampedModel(models.Model):
    created_at = CreationDateTimeField(_("created"))
    modified_at = ModificationDateTimeField(_("modified"))

    def save(self, **kwargs):
        self.update_modified_at = kwargs.pop(
            "update_modified_at", getattr(self, "update_modified_at", True)
        )
        super().save(**kwargs)

    class Meta:
        get_latest_by = "modified_at"
        abstract = True


class UserManager(BaseUserManager):
    """Define a model manager for User model with no username field."""

    use_in_migrations = True

    def _create_user(self, email_address, password, **extra_fields):
        """Create and save a User with the given email_address and password."""
        if not email_address:
            raise ValueError("The given email_address must be set")
        email_address = self.normalize_email(email_address)
        user = self.model(email_address=email_address, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, email_address, password=None, **extra_fields):
        """Create and save a regular User with the given email_address and password."""
        extra_fields.setdefault("is_staff", False)
        extra_fields.setdefault("is_superuser", False)
        return self._create_user(email_address, password, **extra_fields)

    def create_superuser(self, email_address, password, **extra_fields):
        """Create and save a SuperUser with the given email_address and password."""
        extra_fields.setdefault("is_staff", True)
        extra_fields.setdefault("is_superuser", True)

        if extra_fields.get("is_staff") is not True:
            raise ValueError("Superuser must have is_staff=True.")
        if extra_fields.get("is_superuser") is not True:
            raise ValueError("Superuser must have is_superuser=True.")

        return self._create_user(email_address, password, **extra_fields)


class User(AbstractUser, TimeStampedModel):
    username = None
    id = models.UUIDField(primary_key=True, default=uuid4, editable=False)
    email_address = models.EmailField(_("email address"), unique=True)
    objects = UserManager()

    USERNAME_FIELD = "email_address"
    REQUIRED_FIELDS = []
