from typing import List, TypedDict


class UserAttributes(TypedDict):
    Name: str
    Value: str


class GetUserResponse(TypedDict):
    Username: str
    UserAttributes: List[UserAttributes]
