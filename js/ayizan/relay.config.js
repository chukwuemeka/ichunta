import { useMemo } from "react";
import { Environment, RecordSource, Store } from "relay-runtime";
import { Auth } from "aws-amplify";
import "regenerator-runtime/runtime";
import {
  RelayNetworkLayer,
  urlMiddleware,
  authMiddleware,
} from "react-relay-network-modern/node8";
import RelaySSR from 'react-relay-network-modern-ssr/node8/client'

let relayEnvironment;

function createEnvironment(initialRecords) {
  return new Environment({
    network: new RelayNetworkLayer([
      new RelaySSR(initialRecords).getMiddleware({
        lookup: false,
      }),
      urlMiddleware({
        url: (req) => Promise.resolve(process.env.NEXT_PUBLIC_GRAPHQL_URL),
      }),
      authMiddleware({
        token: async (req) => {
          const session = await Auth.currentSession();
          return session?.getAccessToken().getJwtToken();
        },
      }),
    ]),
    store: new Store(new RecordSource()),
  });
}

export function initEnvironment(initialRecords) {
  // Create a network layer from the fetch function
  const environment = relayEnvironment ?? createEnvironment(initialRecords);

  // If your page has Next.js data fetching methods that use Relay, the initial records
  // will get hydrated here
  if (initialRecords) {
    environment.getStore().publish(new RecordSource(initialRecords));
  }
  // For SSG and SSR always create a new Relay environment
  if (typeof window === "undefined") return environment;
  // Create the Relay environment once in the client
  if (!relayEnvironment) relayEnvironment = environment;

  return relayEnvironment;
}

export function useEnvironment(initialRecords) {
  const store = useMemo(() => initEnvironment(initialRecords), [
    initialRecords,
  ]);
  return store;
}
