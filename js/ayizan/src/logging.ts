import winston from "winston";
import {TransformableInfo} from 'logform';
import TransportStream from "winston-transport"

// enumeration to assign color values to
enum LevelColors {
  INFO = 'darkturquoise',
  WARN = 'khaki',
  ERROR = 'tomato',
}

// type levels used for setting color and shutting typescript up
type Levels = 'INFO' | 'WARN' | 'ERROR';

const defaultColor = 'color: inherit';

//! Overriding winston console transporter
class Console extends TransportStream {
  constructor(options = {}) {
    super(options);

    this.setMaxListeners(30);
  }

  log(info: TransformableInfo, next: () => void) {
    // styles a console log statement accordingly to the log level
    // log level colors are taken from levelcolors enum
    const { level, message, ...meta } = info;
    console.log(
      `%c[%c${level.toUpperCase()}%c]:`,
      defaultColor,
      `color: ${LevelColors[level.toUpperCase() as Levels]};`,
      defaultColor,
      // message will be included after stylings
      // through this objects and arrays will be expandable
      message
    );
    console.log(meta)

    // must call the next function here
    // or otherwise you'll only be able to send one message
    next();
  }
}

// creating silent loggers with according levels
// silent by default to be automatically deactivated
// in production mode
export default winston.createLogger({
  transports: [
    new Console({
      silent: false,
      level: 'info',
    }),
  ],
});
