import "../../styles/globals.css";
import { ChakraProvider } from "@chakra-ui/react";
import Amplify, { Auth } from "aws-amplify";
import awsconfig from "amplify.config";
import { AmplifyAuthenticator } from "@aws-amplify/ui-react";
import logger from "src/logging"

Amplify.configure({ ...awsconfig, ssr: true });

function App({ Component, pageProps }) {
  logger.info({message:"Rendering App"})
  return (
      <AmplifyAuthenticator>
        <ChakraProvider>
          <Component {...pageProps} />
        </ChakraProvider>
      </AmplifyAuthenticator>
  );
}

export default App;
