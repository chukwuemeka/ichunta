import Head from "next/head";
import { Box, useDisclosure, useToast } from "@chakra-ui/react";
import NavigationBar from "src/components/NavigationBar";
import Gallery from "src/components/Gallery";
import { RecordMap } from "relay-runtime/lib/store/RelayStoreTypes";

import { useCallback, useEffect, useMemo } from "react";
import UploadMediaModal, {
  UploadMediaFormData,
} from "src/components/UploadMediaModal";
import {
  Environment,
  fetchQuery,
  graphql,
  loadQuery,
  PreloadedQuery,
  useLazyLoadQuery,
  usePreloadedQuery,
  useQueryLoader,
  useRelayEnvironment,
} from "react-relay";
import { useQuery, RelayEnvironmentProvider } from "relay-hooks";
import commitMutation from "relay-commit-mutation-promise";
import { media_CreateMediumMutation } from "__generated__/media_CreateMediumMutation.graphql";
import { Storage } from "aws-amplify";
import { v4 as uuidv4 } from "uuid";
import { mediaQuery } from "__generated__/mediaQuery.graphql";
import { createEnvironment } from "src/relay";
import logger from "src/logging";

const createMediumMutation = graphql`
  mutation media_CreateMediumMutation($input: CreateMediumMutationInput!) {
    createMedium(input: $input) {
      medium {
        id
        area {
          id
        }
      }
    }
  }
`;

async function upload({
  data,
  relayEnvironment,
}: {
  data: UploadMediaFormData;
  relayEnvironment: Environment;
}) {
  const {
    areaName,
    cameraName,
    cameraLatitude,
    cameraLongitude,
    cameraID,
    areaID,
  } = data;

  // Create media
  for (const file of data.files) {
    const s3UploadKey = `uploads/${uuidv4()}.${file.type.split("/")[1]}`;
    await Storage.put(s3UploadKey, file, {
      contentType: file.type,
    });
    await commitMutation<media_CreateMediumMutation>(relayEnvironment, {
      mutation: createMediumMutation,
      variables: {
        input: {
          s3UploadKey: "public/" + s3UploadKey,
          areaID,
          areaName,
          cameraName,
          cameraLatitude,
          cameraLongitude,
          cameraID,
        },
      },
    });
  }
}

const Query = graphql`
  query mediaQuery {
    getCurrentUserAreas(first: 9000)
      @connection(key: "AreaFormGroup_getCurrentUserAreas") {
      edges {
        ...AreaFormGroup_areaSelectOptions
      }
    }
  }
`;

function Media() {
  const data = useQuery<mediaQuery>(
    Query,
    {},
    { fetchPolicy: "store-or-network" }
  );
  const toast = useToast();
  const relayEnvironment = useRelayEnvironment();
  const {
    isOpen: isUploadModalOpen,
    onOpen: onUploadModalOpen,
    onClose: onUploadModalClose,
  } = useDisclosure();
  const handleUploadButtonClick = useCallback(() => {
    onUploadModalOpen();
  }, [onUploadModalOpen]);
  const handleUploadModalValid = useCallback(
    async (data: UploadMediaFormData) => {
      onUploadModalClose();
      await upload({ data, relayEnvironment });
    },
    [relayEnvironment, onUploadModalClose, toast]
  );
  logger.info({ message: "Rendering Media", data });
  return (
    <Box>
      <Head>
        <title>Media</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <NavigationBar
        height="72px"
        onUploadButtonClick={handleUploadButtonClick}
      />
      <Box as="main" marginTop="72px" marginLeft="84px" marginRight="84px">
        {/* <Gallery /> */}
      </Box>
      <UploadMediaModal
        isOpen={isUploadModalOpen}
        onClose={onUploadModalClose}
        onValid={handleUploadModalValid}
        areaFormGroupFragmentRef={data.data?.getCurrentUserAreas?.edges}
      />
    </Box>
  );
}

export default function Page({
  initialRecords,
}: {
  initialRecords?: RecordMap;
}) {
  return (
    <RelayEnvironmentProvider environment={createEnvironment(initialRecords)}>
      <Media />
    </RelayEnvironmentProvider>
  );
}

export async function getServerSideProp() {
  const environment = createEnvironment();
  logger.info({ message: "Created environment", environment });
  await fetchQuery(environment, Query, {}).toPromise();
  return {
    props: { initialRecords: environment.getStore().getSource().toJSON() },
  };
}
