import {
  FormControl,
  FormLabel,
  Input,
  Box,
  Link,
  useBoolean,
} from "@chakra-ui/react";
import { useCallback } from "react";
import { UseFormRegister } from "react-hook-form";
import { UploadMediaFormData } from "./UploadMediaModal";
import Select from "react-select";
import logger from "src/logging";
import { graphql, useFragment } from "react-relay";
import { AreaFormGroup_getCurrentUserAreas_query$key } from "__generated__/AreaFormGroup_getCurrentUserAreas_query.graphql";
import { AreaFormGroup_areaSelectOptions$key } from "__generated__/AreaFormGroup_areaSelectOptions.graphql";

function CreateAreaForm({
  register,
}: {
  register: UseFormRegister<UploadMediaFormData>;
}) {
  return (
    <FormControl isRequired>
      <FormLabel>Area Name</FormLabel>
      <Input defaultValue="Cedar Creek WMA" {...register("areaName")} />
    </FormControl>
  );
}

function SelectAreaForm({
  register,
  fragmentRef,
}: {
  register: UseFormRegister<UploadMediaFormData>;
  fragmentRef?: readonly AreaFormGroup_areaSelectOptions$key[];
}) {
  const data = fragmentRef?.map((f) => {
    return useFragment(
      graphql`
        fragment AreaFormGroup_areaSelectOptions on GetCurrentUserAreasEdge {
          node {
            id
            name
            cameras {
              id
              name
            }
          }
        }
      `,
      f
    );
  });
  const options = (data || [])
    .map((e) => e.node)
    .map((n) => {
      return { label: n.name, value: n.id };
    });

  return (
    <FormControl isRequired>
      <FormLabel>Select an area</FormLabel>
      <Select name="area" options={options} />
    </FormControl>
  );
}

export function AreaFormGroup({
  register,
  fragmentRef,
}: {
  register: UseFormRegister<UploadMediaFormData>;
  fragmentRef?: readonly AreaFormGroup_areaSelectOptions$key[];
}) {
  const [doShowCreateForm, setDoShowCreateForm] = useBoolean(true);
  const handleFormToggleClick = useCallback(() => {
    setDoShowCreateForm.toggle();
  }, [setDoShowCreateForm]);
  return (
    <Box>
      {doShowCreateForm ? (
        <CreateAreaForm register={register} />
      ) : (
        <SelectAreaForm fragmentRef={fragmentRef} register={register} />
      )}

      <Link onClick={handleFormToggleClick}>
        {doShowCreateForm ? "Select an existing area" : "Create a new area"}
      </Link>
    </Box>
  );
}
