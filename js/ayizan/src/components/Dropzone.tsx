import { Input, Flex, Box, Image, SimpleGrid } from "@chakra-ui/react";
import { useCallback, useEffect, useState } from "react";
import { useDropzone } from "react-dropzone";
import { v4 as uuidv4 } from "uuid";

export default function Dropzone({
  onChange,
}: {
  onChange?: (files: Array<File>) => void;
}) {
  const [isDirty, setIsDirty] = useState<boolean>(false);
  const [files, setFiles] = useState<
    Array<File & { previewURL: string; id: string }>
  >([]);
  const onDrop = useCallback(
    (acceptedFiles: Array<File>) => {
      setIsDirty(true);
      setFiles([
        ...files,
        ...acceptedFiles.map((file: File) =>
          Object.assign(file, {
            previewURL: URL.createObjectURL(file),
            id: uuidv4(),
          })
        ),
      ]);
    },
    [setFiles, files]
  );
  const onFileDialogCancel = useCallback(() => {
    setIsDirty(true);
  }, [setIsDirty]);
  const { getRootProps, getInputProps } = useDropzone({
    onDrop,
    onFileDialogCancel,
    accept: "image/*",
  });
  useEffect(() => {
    if (onChange && isDirty) {
      onChange(files);
    }
  }, [files, onChange, isDirty]);
  return (
    <Box>
      <Flex
        {...getRootProps({
          alignItems: "center",
          padding: "20px",
          borderWidth: "2px",
          borderRadius: "2px",
          borderColor: "#eee",
          borderStyle: "dashed",
          backgroundColor: "#fafafa",
          color: "#bdbdbd",
          transition: "border .24s ease-in-out",
        })}
      >
        <Input {...getInputProps()} size="sm" />
        <p>Drag 'n' drop some files here, or click to select files</p>
      </Flex>
      <Box
        as="aside"
        marginTop={4}
        marginBottom={4}
        maxHeight="200px"
        overflowY="scroll"
      >
        <SimpleGrid columns={4} spacing={2}>
          {files.map((file) => (
            <Image key={file.id} src={file.previewURL} />
          ))}
        </SimpleGrid>
      </Box>
    </Box>
  );
}
