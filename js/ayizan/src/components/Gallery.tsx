import { SimpleGrid, Flex, Img } from "@chakra-ui/react";
import Carousel from "src/components/Carousel";
import { useCallback, useState } from "react";
import { graphql, usePaginationFragment } from "react-relay";

const imageURLs = [...Array(200).keys()].map(() => {
  return `https://picsum.photos/id/${Math.floor(Math.random() * 50)}/1000/1000`;
});

function Media({
  onClick,
}: {
  onClick: React.MouseEventHandler<HTMLImageElement> | undefined;
}) {
  const idx = Math.floor(Math.random() * imageURLs.length);
  return (
    <Img
      onClick={onClick}
      boxSize="200px"
      objectFit="cover"
      src={imageURLs[idx]}
      cursor="pointer"
    />
  );
}

export default function Gallery() {
  const [isCarouselVisible, setIsCarouselVisible] = useState(false);
  const handleMediaClick = useCallback(() => {
    setIsCarouselVisible(true);
  }, [setIsCarouselVisible]);

  return (
    <Flex>
      <SimpleGrid columns={8} spacing={10}>
        {[...Array(200).keys()].map((idx) => (
          <Media key={idx} onClick={handleMediaClick} />
        ))}
      </SimpleGrid>
      <Carousel
        isVisible={isCarouselVisible}
        initialURL={imageURLs[0]}
        urls={imageURLs}
      ></Carousel>
    </Flex>
  );
}
