import {
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalFooter,
  ModalBody,
  ModalCloseButton,
  FormControl,
  FormLabel,
  FormErrorMessage,
  Input,
  Button,
  Box,
} from "@chakra-ui/react";
import { ReactNode, useCallback } from "react";
import { useForm, UseFormRegister } from "react-hook-form";
import Dropzone from "src/components/Dropzone";
import { AreaFormGroup_areaSelectOptions$key } from "__generated__/AreaFormGroup_areaSelectOptions.graphql";
import { AreaFormGroup } from "./AreaFormGroup";

export interface UploadMediaFormData {
  files: Array<File>;
  cameraName?: string;
  cameraID?: string;
  cameraLatitude?: number;
  cameraLongitude?: number;
  areaName?: string;
  areaID?: string;
}

interface Camera {
  id: string;
  name: string;
}
interface Area {
  id: string;
  name: string;
  cameras: Array<Camera>;
}

function CameraFormGroup({
  register,
}: {
  register: UseFormRegister<UploadMediaFormData>;
}) {
  return (
    <>
      <FormControl isRequired>
        <FormLabel>Camera Name</FormLabel>
        <Input defaultValue="Alpha" {...register("cameraName")} />
      </FormControl>
      <FormControl isRequired>
        <FormLabel>Camera Latitude</FormLabel>
        <Input defaultValue={0} type="number" {...register("cameraLatitude")} />
      </FormControl>
      <FormControl isRequired>
        <FormLabel>Camera Longitude</FormLabel>
        <Input
          defaultValue={0}
          type="number"
          {...register("cameraLongitude")}
        />
      </FormControl>
    </>
  );
}

function FormGroupContainer({ children }: { children: ReactNode }) {
  return <Box paddingBottom={7}>{children}</Box>;
}

function UploadMediaForm({
  onValid,
  onClose,
  areaFormGroupFragmentRef,
}: {
  onValid: (data: UploadMediaFormData) => any;
  onClose: () => void;
  areaFormGroupFragmentRef?: readonly AreaFormGroup_areaSelectOptions$key[];
}) {
  const {
    handleSubmit,
    setValue,
    setError,
    trigger,
    register,
    formState: { errors, isValid },
  } = useForm<UploadMediaFormData>();
  const handleDropzoneChange = useCallback(
    (files: Array<File>) => {
      if (!files || files.length == 0) {
        setError("files", { message: "notmatch" });
      } else {
        setValue("files", files);
      }
      trigger("files");
    },
    [setValue]
  );
  return (
    <form
      onSubmit={handleSubmit((data: UploadMediaFormData) => {
        if (data?.cameraLatitude) {
          data.cameraLatitude = parseFloat(
            (data.cameraLatitude as unknown) as string
          );
        }
        if (data?.cameraLongitude) {
          data.cameraLongitude = parseFloat(
            (data.cameraLongitude as unknown) as string
          );
        }
        onValid(data);
      })}
    >
      <ModalBody pb={6}>
        <FormGroupContainer>
          <AreaFormGroup
            register={register}
            fragmentRef={areaFormGroupFragmentRef}
          />
        </FormGroupContainer>
        <FormGroupContainer>
          <CameraFormGroup register={register} />
        </FormGroupContainer>
        <FormGroupContainer>
          <FormControl isInvalid={!!errors.files}>
            <FormLabel>Media</FormLabel>
            <Dropzone onChange={handleDropzoneChange} />
            <FormErrorMessage>
              {errors.files && errors.files.message}
            </FormErrorMessage>
          </FormControl>
        </FormGroupContainer>
      </ModalBody>
      <ModalFooter>
        <Button colorScheme="blue" mr={3} type="submit" isDisabled={!isValid}>
          Submit
        </Button>
        <Button onClick={onClose}>Cancel</Button>
      </ModalFooter>
    </form>
  );
}

export default function UploadMediaModal({
  isOpen,
  onClose,
  onValid,
  areaFormGroupFragmentRef,
}: {
  isOpen: boolean;
  onClose: () => void;
  onValid: (data: UploadMediaFormData) => any;
  areaFormGroupFragmentRef?: readonly AreaFormGroup_areaSelectOptions$key[];
}) {
  return (
    <Modal isOpen={isOpen} onClose={onClose} size="3xl">
      <ModalOverlay />
      <ModalContent>
        <ModalHeader>Upload your media</ModalHeader>
        <ModalCloseButton />
        <UploadMediaForm
          onValid={onValid}
          onClose={onClose}
          areaFormGroupFragmentRef={areaFormGroupFragmentRef}
        />
      </ModalContent>
    </Modal>
  );
}
