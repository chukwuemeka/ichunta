import { Box, Flex, Link, Icon } from "@chakra-ui/react";
import { MouseEventHandler } from "react";
import { FiUpload } from "react-icons/fi";

export default function NavigationBar({
  height,
  onUploadButtonClick,
}: {
  height: number | string;
  onUploadButtonClick?: MouseEventHandler<HTMLAnchorElement>;
}) {
  return (
    <Box
      as="header"
      top="0px"
      left="0px"
      right="0px"
      position="sticky"
      background="tomato"
      height={height}
    >
      <Flex
        maxWidth="1200px"
        height="4.5em"
        marginInlineStart="auto"
        marginInlineEnd="auto"
        alignItems="center"
        justifyContent="flex-end"
      >
        <Link color="white" onClick={onUploadButtonClick}>
          <Icon mx="2px" as={FiUpload} /> Upload
        </Link>
      </Flex>
    </Box>
  );
}
