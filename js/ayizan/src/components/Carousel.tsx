import { Box } from "@chakra-ui/react";
import { useState } from "react";

function Details() {
  return (
    <Box
      background="gray"
      position="absolute"
      top={0}
      bottom={0}
      right={0}
      width="360px"
    ></Box>
  );
}

export default function Carousel({
  isVisible,
  initialURL,
  urls,
}: {
  isVisible: boolean;
  initialURL: string;
  urls: Array<string>;
}) {
  const [selectedURL, setSelectedURL] = useState(initialURL);
  return (
    <Box
      display={isVisible ? "block" : "none"}
      position="fixed"
      background="black"
      top={0}
      bottom={0}
      left={0}
      right={0}
      zIndex={0}
      overflow="hidden"
    >
      <Box
        position="absolute"
        top={0}
        bottom={0}
        left={0}
        backgroundPosition="center"
        backgroundRepeat="no-repeat"
        backgroundSize="contain"
        right={"360px"}
        zIndex={0}
        overflow="hidden"
        backgroundImage={`url(${selectedURL})`}
      />
      <Details />
    </Box>
  );
}
