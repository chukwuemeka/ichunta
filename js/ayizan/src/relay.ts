import { RecordMap } from "relay-runtime/lib/store/RelayStoreTypes";
import { Environment, RecordSource, Store } from "relay-runtime";
import { Auth } from "aws-amplify";
import "regenerator-runtime/runtime";
import {
  RelayNetworkLayer,
  urlMiddleware,
  authMiddleware,
} from "react-relay-network-modern";
import logger from "src/logging";

export function createEnvironment(initialRecords?: RecordMap) {
  return new Environment({
    network: new RelayNetworkLayer([
      urlMiddleware({
        url: (req) => Promise.resolve(process.env.NEXT_PUBLIC_GRAPHQL_URL!),
      }),
      authMiddleware({
        token: async (req) => {
          try {
            const session = await Auth.currentSession();
            logger.info({ message: "Loaded session", session });
            return session?.getAccessToken().getJwtToken();
          } catch (error) {
            logger.error({message: "Failed to fetch session", error})
          }
          return ""          
        },
      }),
    ]),
    store: new Store(new RecordSource(initialRecords)),
  });
}
