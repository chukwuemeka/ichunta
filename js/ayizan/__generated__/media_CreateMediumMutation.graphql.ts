/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { ConcreteRequest } from "relay-runtime";
export type CreateMediumMutationInput = {
  areaID?: string | null;
  areaName?: string | null;
  cameraID?: string | null;
  cameraName?: string | null;
  cameraLatitude?: number | null;
  cameraLongitude?: number | null;
  s3UploadKey: string;
  clientMutationId?: string | null;
};
export type media_CreateMediumMutationVariables = {
  input: CreateMediumMutationInput;
};
export type media_CreateMediumMutationResponse = {
  readonly createMedium: {
    readonly medium: {
      readonly id: string;
      readonly area: {
        readonly id: string;
      };
    } | null;
  } | null;
};
export type media_CreateMediumMutation = {
  readonly response: media_CreateMediumMutationResponse;
  readonly variables: media_CreateMediumMutationVariables;
};

/*
mutation media_CreateMediumMutation(
  $input: CreateMediumMutationInput!
) {
  createMedium(input: $input) {
    medium {
      id
      area {
        id
      }
    }
  }
}
*/

const node: ConcreteRequest = (function () {
  var v0 = [
      {
        defaultValue: null,
        kind: "LocalArgument",
        name: "input",
      },
    ],
    v1 = {
      alias: null,
      args: null,
      kind: "ScalarField",
      name: "id",
      storageKey: null,
    },
    v2 = [
      {
        alias: null,
        args: [
          {
            kind: "Variable",
            name: "input",
            variableName: "input",
          },
        ],
        concreteType: "CreateMediumMutationPayload",
        kind: "LinkedField",
        name: "createMedium",
        plural: false,
        selections: [
          {
            alias: null,
            args: null,
            concreteType: "Medium",
            kind: "LinkedField",
            name: "medium",
            plural: false,
            selections: [
              v1 /*: any*/,
              {
                alias: null,
                args: null,
                concreteType: "Area",
                kind: "LinkedField",
                name: "area",
                plural: false,
                selections: [v1 /*: any*/],
                storageKey: null,
              },
            ],
            storageKey: null,
          },
        ],
        storageKey: null,
      },
    ];
  return {
    fragment: {
      argumentDefinitions: v0 /*: any*/,
      kind: "Fragment",
      metadata: null,
      name: "media_CreateMediumMutation",
      selections: v2 /*: any*/,
      type: "Mutation",
      abstractKey: null,
    },
    kind: "Request",
    operation: {
      argumentDefinitions: v0 /*: any*/,
      kind: "Operation",
      name: "media_CreateMediumMutation",
      selections: v2 /*: any*/,
    },
    params: {
      cacheID: "7c22156fa0ae90b9b627a9e55f5d7df7",
      id: null,
      metadata: {},
      name: "media_CreateMediumMutation",
      operationKind: "mutation",
      text:
        "mutation media_CreateMediumMutation(\n  $input: CreateMediumMutationInput!\n) {\n  createMedium(input: $input) {\n    medium {\n      id\n      area {\n        id\n      }\n    }\n  }\n}\n",
    },
  };
})();
(node as any).hash = "0aa9ead0c0ff9142906fad8b2ed6e0d2";
export default node;
