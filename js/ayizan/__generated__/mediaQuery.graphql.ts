/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { ConcreteRequest } from "relay-runtime";
import { FragmentRefs } from "relay-runtime";
export type mediaQueryVariables = {};
export type mediaQueryResponse = {
  readonly getCurrentUserAreas: {
    readonly edges: ReadonlyArray<{
      readonly " $fragmentRefs": FragmentRefs<"AreaFormGroup_areaSelectOptions">;
    }>;
  };
};
export type mediaQuery = {
  readonly response: mediaQueryResponse;
  readonly variables: mediaQueryVariables;
};

/*
query mediaQuery {
  getCurrentUserAreas(first: 9000) {
    edges {
      ...AreaFormGroup_areaSelectOptions
      cursor
      node {
        __typename
        id
      }
    }
    pageInfo {
      endCursor
      hasNextPage
    }
  }
}

fragment AreaFormGroup_areaSelectOptions on GetCurrentUserAreasEdge {
  node {
    id
    name
    cameras {
      id
      name
    }
  }
}
*/

const node: ConcreteRequest = (function () {
  var v0 = {
      alias: null,
      args: null,
      kind: "ScalarField",
      name: "cursor",
      storageKey: null,
    },
    v1 = {
      alias: null,
      args: null,
      kind: "ScalarField",
      name: "__typename",
      storageKey: null,
    },
    v2 = {
      alias: null,
      args: null,
      concreteType: "PageInfo",
      kind: "LinkedField",
      name: "pageInfo",
      plural: false,
      selections: [
        {
          alias: null,
          args: null,
          kind: "ScalarField",
          name: "endCursor",
          storageKey: null,
        },
        {
          alias: null,
          args: null,
          kind: "ScalarField",
          name: "hasNextPage",
          storageKey: null,
        },
      ],
      storageKey: null,
    },
    v3 = [
      {
        kind: "Literal",
        name: "first",
        value: 9000,
      },
    ],
    v4 = {
      alias: null,
      args: null,
      kind: "ScalarField",
      name: "id",
      storageKey: null,
    },
    v5 = {
      alias: null,
      args: null,
      kind: "ScalarField",
      name: "name",
      storageKey: null,
    };
  return {
    fragment: {
      argumentDefinitions: [],
      kind: "Fragment",
      metadata: null,
      name: "mediaQuery",
      selections: [
        {
          alias: "getCurrentUserAreas",
          args: null,
          concreteType: "GetCurrentUserAreasConnection",
          kind: "LinkedField",
          name: "__AreaFormGroup_getCurrentUserAreas_connection",
          plural: false,
          selections: [
            {
              alias: null,
              args: null,
              concreteType: "GetCurrentUserAreasEdge",
              kind: "LinkedField",
              name: "edges",
              plural: true,
              selections: [
                v0 /*: any*/,
                {
                  alias: null,
                  args: null,
                  concreteType: "Area",
                  kind: "LinkedField",
                  name: "node",
                  plural: false,
                  selections: [v1 /*: any*/],
                  storageKey: null,
                },
                {
                  args: null,
                  kind: "FragmentSpread",
                  name: "AreaFormGroup_areaSelectOptions",
                },
              ],
              storageKey: null,
            },
            v2 /*: any*/,
          ],
          storageKey: null,
        },
      ],
      type: "Query",
      abstractKey: null,
    },
    kind: "Request",
    operation: {
      argumentDefinitions: [],
      kind: "Operation",
      name: "mediaQuery",
      selections: [
        {
          alias: null,
          args: v3 /*: any*/,
          concreteType: "GetCurrentUserAreasConnection",
          kind: "LinkedField",
          name: "getCurrentUserAreas",
          plural: false,
          selections: [
            {
              alias: null,
              args: null,
              concreteType: "GetCurrentUserAreasEdge",
              kind: "LinkedField",
              name: "edges",
              plural: true,
              selections: [
                {
                  alias: null,
                  args: null,
                  concreteType: "Area",
                  kind: "LinkedField",
                  name: "node",
                  plural: false,
                  selections: [
                    v4 /*: any*/,
                    v5 /*: any*/,
                    {
                      alias: null,
                      args: null,
                      concreteType: "Camera",
                      kind: "LinkedField",
                      name: "cameras",
                      plural: true,
                      selections: [v4 /*: any*/, v5 /*: any*/],
                      storageKey: null,
                    },
                    v1 /*: any*/,
                  ],
                  storageKey: null,
                },
                v0 /*: any*/,
              ],
              storageKey: null,
            },
            v2 /*: any*/,
          ],
          storageKey: "getCurrentUserAreas(first:9000)",
        },
        {
          alias: null,
          args: v3 /*: any*/,
          filters: null,
          handle: "connection",
          key: "AreaFormGroup_getCurrentUserAreas",
          kind: "LinkedHandle",
          name: "getCurrentUserAreas",
        },
      ],
    },
    params: {
      cacheID: "d8ec1c39404fe1629a007dc4652ebaaa",
      id: null,
      metadata: {
        connection: [
          {
            count: null,
            cursor: null,
            direction: "forward",
            path: ["getCurrentUserAreas"],
          },
        ],
      },
      name: "mediaQuery",
      operationKind: "query",
      text:
        "query mediaQuery {\n  getCurrentUserAreas(first: 9000) {\n    edges {\n      ...AreaFormGroup_areaSelectOptions\n      cursor\n      node {\n        __typename\n        id\n      }\n    }\n    pageInfo {\n      endCursor\n      hasNextPage\n    }\n  }\n}\n\nfragment AreaFormGroup_areaSelectOptions on GetCurrentUserAreasEdge {\n  node {\n    id\n    name\n    cameras {\n      id\n      name\n    }\n  }\n}\n",
    },
  };
})();
(node as any).hash = "b09a1bb25bcd028411284fce261553f9";
export default node;
