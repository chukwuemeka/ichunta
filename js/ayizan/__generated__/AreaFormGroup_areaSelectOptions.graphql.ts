/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { ReaderFragment } from "relay-runtime";
import { FragmentRefs } from "relay-runtime";
export type AreaFormGroup_areaSelectOptions = {
  readonly node: {
    readonly id: string;
    readonly name: string;
    readonly cameras: ReadonlyArray<{
      readonly id: string;
      readonly name: string;
    }>;
  };
  readonly " $refType": "AreaFormGroup_areaSelectOptions";
};
export type AreaFormGroup_areaSelectOptions$data = AreaFormGroup_areaSelectOptions;
export type AreaFormGroup_areaSelectOptions$key = {
  readonly " $data"?: AreaFormGroup_areaSelectOptions$data;
  readonly " $fragmentRefs": FragmentRefs<"AreaFormGroup_areaSelectOptions">;
};

const node: ReaderFragment = (function () {
  var v0 = {
      alias: null,
      args: null,
      kind: "ScalarField",
      name: "id",
      storageKey: null,
    },
    v1 = {
      alias: null,
      args: null,
      kind: "ScalarField",
      name: "name",
      storageKey: null,
    };
  return {
    argumentDefinitions: [],
    kind: "Fragment",
    metadata: null,
    name: "AreaFormGroup_areaSelectOptions",
    selections: [
      {
        alias: null,
        args: null,
        concreteType: "Area",
        kind: "LinkedField",
        name: "node",
        plural: false,
        selections: [
          v0 /*: any*/,
          v1 /*: any*/,
          {
            alias: null,
            args: null,
            concreteType: "Camera",
            kind: "LinkedField",
            name: "cameras",
            plural: true,
            selections: [v0 /*: any*/, v1 /*: any*/],
            storageKey: null,
          },
        ],
        storageKey: null,
      },
    ],
    type: "GetCurrentUserAreasEdge",
    abstractKey: null,
  };
})();
(node as any).hash = "5edbca9ed975e96016e72d20b101fc29";
export default node;
