resource "aws_s3_bucket" "development" {
  bucket = "ichunta-development"
  acl    = "private"
  tags = {
    "environment" = "development"
  }

  cors_rule {
    allowed_headers = ["*"]
    allowed_methods = ["PUT", "POST"]
    allowed_origins = ["*"]
    max_age_seconds = 3000
    expose_headers  = ["ETag"]
  }
}

resource "aws_s3_access_point" "development" {
  bucket = aws_s3_bucket.development.id
  name   = "ichunta-development-web"
}

resource "aws_cognito_identity_pool" "development" {
  identity_pool_name               = "ichunta-development"
  allow_unauthenticated_identities = false

  cognito_identity_providers {
    client_id     = aws_cognito_user_pool_client.web_development.id
    provider_name = aws_cognito_user_pool.development.endpoint
  }

  tags = {
    "environment" = "development"
  }

}

resource "aws_iam_role_policy" "cognito_authenticated_development" {
  name = "ichunta-development-cognito-authenticated"
  role = aws_iam_role.cognito_authenticated_development.id

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        "s3:PutObject",
        "s3:PutObjectAcl"
      ],
      "Resource": [
        "${aws_s3_access_point.development.arn}/*"
      ]
    },
        {
      "Effect": "Allow",
      "Action": [
        "s3:PutObject",
        "s3:PutObjectAcl"
      ],
      "Resource": [
        "${aws_s3_bucket.development.arn}/public/uploads/*"
      ]
    }

  ]
}
EOF
}
resource "aws_cognito_identity_pool_roles_attachment" "web_development" {
  identity_pool_id = aws_cognito_identity_pool.development.id

  role_mapping {
    identity_provider         = "${aws_cognito_user_pool.development.endpoint}:${aws_cognito_user_pool_client.web_development.id}"
    ambiguous_role_resolution = "AuthenticatedRole"
    type                      = "Token"
  }

  roles = {
    "authenticated" = aws_iam_role.cognito_authenticated_development.arn
  }
}

resource "aws_iam_role" "cognito_authenticated_development" {
  name = "ichunta-development-cognito-authenticated"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Federated": "cognito-identity.amazonaws.com"
      },
      "Action": "sts:AssumeRoleWithWebIdentity",
      "Condition": {
        "StringEquals": {
          "cognito-identity.amazonaws.com:aud": "${aws_cognito_identity_pool.development.id}"
        },
        "ForAnyValue:StringLike": {
          "cognito-identity.amazonaws.com:amr": "authenticated"
        }
      }
    }
  ]
}
EOF

  tags = {
    "environment" = "development"
  }

}

resource "aws_cognito_user_pool" "development" {
  name                     = "ichunta-development"
  username_attributes      = ["email"]
  auto_verified_attributes = ["email"]

  password_policy {
    minimum_length    = 6
    require_lowercase = false
    require_numbers   = false
    require_symbols   = false
    require_uppercase = false
  }
  verification_message_template {
    default_email_option = "CONFIRM_WITH_CODE"
  }
  device_configuration {
    challenge_required_on_new_device      = true
    device_only_remembered_on_user_prompt = true
  }
  account_recovery_setting {
    recovery_mechanism {
      name     = "verified_email"
      priority = 1
    }
  }

  tags = {
    "environment" = "development"
  }
}

resource "aws_cognito_user_pool_client" "web_development" {
  name                                 = "ichunta-development-web"
  user_pool_id                         = aws_cognito_user_pool.development.id
  allowed_oauth_flows                  = ["code"]
  allowed_oauth_scopes                 = ["phone", "email", "openid", "profile"]
  callback_urls                        = ["http://${var.ayizan_host_development}/"]
  logout_urls                          = ["http://${var.ayizan_host_development}/"]
  supported_identity_providers         = ["COGNITO"]
  allowed_oauth_flows_user_pool_client = true

}

resource "aws_cognito_user_pool_domain" "development" {
  domain       = "ichunta-development"
  user_pool_id = aws_cognito_user_pool.development.id
}

output "aws_cognito_user_pool_id_development" {
  value = aws_cognito_user_pool.development.id
}

output "aws_cognito_user_pool_web_client_id_development" {
  value = aws_cognito_user_pool_client.web_development.id
}

output "aws_cognito_user_pool_endpoint_development" {
  value = aws_cognito_user_pool.development.endpoint
}

output "aws_cognito_identity_pool_id_development" {
  value = aws_cognito_identity_pool.development.id
}

output "aws_s3_access_point_arn_development" {
  value = aws_s3_access_point.development.arn
}

output "aws_s3_bucket_arn_development" {
  value = aws_s3_bucket.development.arn
}
